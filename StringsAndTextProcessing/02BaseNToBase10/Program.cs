﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace _02BaseNToBase10
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, int> weights = new Dictionary<int, int>();

            for (int i = 0; i < 10; i++)
            {
                weights[i.ToString().First()] = i;
                weights['A' + i] = i + 10;
            }


            string[] split = Console.ReadLine().Split();
            int fromBase = int.Parse(split.First());
            string source = split.Last();

            BigInteger result = 0;

            BigInteger current = 1;
            for (int i = source.Length - 1; i >= 0; i--)
            {
                result += current * weights[source[i]];
                current *= fromBase;
            }

            Console.WriteLine(result);
        }
    }
}
