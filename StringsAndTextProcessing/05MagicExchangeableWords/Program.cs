﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _05MagicExchangeableWords
{
    class Program
    {
        static bool Check(string a, string b)
        {
            string longer = a, shorter = b;
            if (a.Length < b.Length)
            {
                longer = b;
                shorter = a;
            }

            Dictionary<char, char> correspondsTo = new Dictionary<char, char>();


            for (int i = 0; i < shorter.Length; i++)
            {
                char c1 = a[i], c2 = b[i];
                if (!correspondsTo.ContainsKey(c1))
                {
                    correspondsTo[c1] = c2;
                }
                else if (correspondsTo[c1] != c2)
                {
                    return false;
                }
            }

            return
                a.Length == b.Length ||
                a.Distinct().Count() == b.Distinct().Count();
        }
        static void Main(string[] args)
        {
            string[] split = Console.ReadLine().Split();
            Console.WriteLine(Check(split.First(), split.Last()).ToString().ToLower());
        }
    }
}
