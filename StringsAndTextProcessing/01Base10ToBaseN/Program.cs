﻿using System;
using System.Linq;
using System.Numerics;
using System.Text;

namespace _01Base10ToBaseN
{
    class Program
    {
        static readonly string Alphabet = "0123456789ABCDEF";
        static void Main(string[] args)
        {
            string[] split = Console.ReadLine().Split();
            BigInteger toBase = BigInteger.Parse(split.First());
            BigInteger n = BigInteger.Parse(split.Last());
            ;

            StringBuilder builder = new StringBuilder();

            if (n == 0)
            {
                builder.Append("0");
            }
            else
            {
                while(n!=0)
                {
                    builder.Append(Alphabet[(int)(n % toBase)]);
                    n /= toBase;
                }
            }

            Console.WriteLine(new string(builder.ToString().Reverse().ToArray()));
        }
    }
}
