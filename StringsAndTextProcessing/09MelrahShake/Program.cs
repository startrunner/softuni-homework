﻿using System;

namespace _09MelrahShake
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = Console.ReadLine();
            string pattern = Console.ReadLine();

            for(;;)
            {
                int firstMatch = text.IndexOf(pattern);
                int lastMatch = text.LastIndexOf(pattern);

                if (string.IsNullOrEmpty(pattern) || firstMatch == -1 || lastMatch == -1 || firstMatch == lastMatch)
                {
                    Console.WriteLine("No shake.");
                    Console.WriteLine(text);
                    break;
                }
                else
                {
                    Console.WriteLine("Shaked it.");
                    text = text.Remove(firstMatch, pattern.Length);
                    lastMatch -= pattern.Length;
                    text = text.Remove(lastMatch, pattern.Length);
                    pattern = pattern.Remove(pattern.Length / 2, 1);
                }
            }
        }
    }
}
