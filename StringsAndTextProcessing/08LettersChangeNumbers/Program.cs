﻿using System;
using System.Linq;

namespace _08LettersChangeNumbers
{
    class Program
    {
        static double Process(string input)
        {
            char before = input.First();
            char after = input.Last();
            double number = double.Parse(input.Substring(1, input.Length - 2));
            ;
            if (char.IsUpper(before))
            {
                number /= (before - 'A' + 1);
            }
            else
            {
                number *= (before - 'a' + 1);
            }

            if (char.IsUpper(after))
            {
                number -= (after - 'A' + 1);
            }
            else
            {
                number += (after - 'a' + 1);
            }

            return number;
        }
        static void Main(string[] args)
        {
            Console.WriteLine(
                Console
                    .ReadLine()
                    .Split("".ToArray(), StringSplitOptions.RemoveEmptyEntries)
                    .Select(Process)
                    .Sum()
                    .ToString("F2")
            );
        }
    }
}
