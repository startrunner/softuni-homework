﻿using System;
using System.Linq;
using System.Numerics;

namespace _04CharacterMultiplier
{
    class Program
    {
        static BigInteger DoShit(string a, string b)
        {
            int len = Math.Max(a.Length, b.Length);

            BigInteger result = 0;

            for(int i=0;i<len;i++)
            {
                int x = 1;

                if(i<a.Length)
                {
                    x *= a[i];
                }
                if(i<b.Length)
                {
                    x *= b[i];
                }

                result += x;
            }

            return result;
        }

        static void Main(string[] args)
        {
            string[] split = Console.ReadLine().Split();
            Console.WriteLine(DoShit(split.First(), split.Last()));
        }
    }
}
