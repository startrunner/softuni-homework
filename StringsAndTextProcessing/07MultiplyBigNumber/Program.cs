﻿using System;
using System.Linq;

namespace _07MultiplyBigNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = Console
                .ReadLine()
                .Where(char.IsDigit)
                .Select(x => x - '0')
                .SkipWhile(x => x == 0)
                .ToArray();

            int b = int.Parse(Console.ReadLine());

            int over = 0;
            for(int i=a.Length-1;i>=0;i--)
            {
                a[i] = a[i] * b + over;

                over = a[i] / 10;
                a[i] %= 10;
            }

            if (over != 0)
            {
                Console.Write(over);
                foreach (int x in a) Console.Write(x);
            }
            else
            {
                a = a.SkipWhile(x => x == 0).ToArray();
                if (!a.Any())
                {
                    Console.Write("0");
                }
                else
                {
                    foreach (int x in a) Console.Write(x);
                }
            }
            Console.WriteLine();
        }
    }
}
