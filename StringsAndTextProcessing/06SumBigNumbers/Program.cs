﻿using System;
using System.Linq;

namespace _06SumBigNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = Console.ReadLine().Where(char.IsDigit).Select(x => x - '0').SkipWhile(x => x == 0).Cast<int>().ToArray();
            int[] b = Console.ReadLine().Where(char.IsDigit).Select(x => x - '0').SkipWhile(x => x == 0).Cast<int>().ToArray();
            int[] sum = new int[Math.Max(a.Length, b.Length)];

            int over = 0;
            for (int i = sum.Length - 1, j = 1; i >= 0; i--, j++)
            {
                int current = a.ElementAtOrDefault(a.Length - j) + b.ElementAtOrDefault(b.Length - j) + over;
                over = current / 10;
                sum[i] = (current % 10);
            }

            if (over != 0)
            {
                Console.Write(over);
            }
            foreach (byte x in sum) Console.Write(x);
            Console.WriteLine();
        }
    }
}
