﻿using System;
using System.Linq;

namespace _03UnicodeCharacters
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(
                string.Join(
                    "",
                    Console
                        .ReadLine()
                        .Select(x=>(int)x)
                        .Select(x=>$"\\u{Convert.ToString(x, 16).PadLeft(4, '0')}")
                )
            );
        }
    }
}
