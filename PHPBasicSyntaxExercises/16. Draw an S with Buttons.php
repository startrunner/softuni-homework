<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>First Steps Into PHP</title>
</head>
<body>
<?php
for($i=1;$i<=9;$i++)
{
    for($j=1;$j<=5;$j++)
    {
        $blu=false;
        if($i==1 || $i==9 || $i==5 || ($i<=5 && $j==1) || ($i>5 && $j==5))
        {
            $blu=true;
        }

        $style='';
        if($blu)
        {
            $style='style="background-color: blue;"';
        }

        echo '<button '.$style.' >';
        printf("%d", (int)$blu);
        echo '</button>';

        if($j==5)echo '<br/>';
    }
}
?>
</body>
</html>