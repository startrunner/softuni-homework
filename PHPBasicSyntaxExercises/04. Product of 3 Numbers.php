<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>First Steps Into PHP</title>

</head>
<body>
    <form>
        X: <input type="text" name="num1" />
		Y: <input type="text" name="num2" />
        Z: <input type="text" name="num3" />
		<input type="submit" />
    </form>
    <?php
    if(!isset($_GET['num1']))
    {
        exit(0);
    }

    $num1=$_GET['num1'];
    $num2=$_GET['num2'];
    $num3=$_GET['num3'];

    $neg=0;

    if($num1<0)$neg++;
    if($num2<0)$neg++;
    if($num3<0)$neg++;

    if($neg==0 || $neg==2 || $num1==0 || $num2==0 || $num3==0)
    {
        echo 'positive';
    }
    else
    {
        echo 'negative';
    }

    ?>
</body>
</html>