<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>First Steps Into PHP</title>

</head>
<body>
    <form>
        N: <input type="text" name="num" />
        <input type="submit" />
    </form>
	<?php

    if(!isset($_GET['num']))exit(0);

    $n=$_GET['num']+0;

    $sieve = array_fill(2, $n+0, 1);

    for($i=2;$i<=$n;$i++)
    {
        if($sieve[$i])
        {
            for($j=2*$i;$j<=$n;$j+=$i)
            {
                $sieve[$j]=0;
            }
        }
    }

    for($i=$n;$i>1;$i = $i-1)
    {
        if($sieve[$i])printf("%d ", $i);
    }

    echo '';

    ?>
</body>
</html>