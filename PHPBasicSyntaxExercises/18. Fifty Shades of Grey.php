<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>First Steps Into PHP</title>
    <style>
        div {
            display: inline-block;
            margin: 5px;
            width: 20px;
            height: 20px;
        }
    </style>
</head>
<body>
<?php
for($i=1;$i<=50;$i++)
{
    $shade  = (int)(($i-1)/10)*51 + (($i-1)%10)*5;

    printf("<div style='background-color: rgb(%d, %d, %d)'></div>", $shade, $shade, $shade);

    if($i%10==0)echo  '<br/>';
}
?>
</body>
</html>