﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _07PopulationCounter
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, Dictionary<string, ulong>> statistics = new Dictionary<string, Dictionary<string, ulong>>();
            Dictionary<string, ulong> totalPopulation = new Dictionary<string, ulong>();

            for(;;)
            {
                string line = Console.ReadLine();
                if (line == "report") break;

                string[] split = line.Split('|');

                string country = split[1];
                string city = split[0];
                ulong population = ulong.Parse(split[2]);

                if(!statistics.ContainsKey(country))
                {
                    statistics[country] = new Dictionary<string, ulong>();
                    totalPopulation[country] = population;
                }
                else
                {
                    totalPopulation[country] += population;
                }

                if(!statistics[country].ContainsKey(city))
                {
                    statistics[country][city] = population;
                }
                else
                {
                    statistics[country][city] += population;
                }
            }

            foreach(var country in statistics.OrderByDescending(x=>totalPopulation[x.Key]))
            {
                Console.WriteLine($"{country.Key} (total population: {totalPopulation[country.Key]})");
                foreach(var city in country.Value.OrderByDescending(x=>x.Value))
                {
                    Console.WriteLine($"=>{city.Key}: {city.Value}");
                }
            }
        }
    }
}
