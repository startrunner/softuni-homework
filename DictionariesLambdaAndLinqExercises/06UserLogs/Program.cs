﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _06UserLogs
{
    class Program
    {

        static IReadOnlyDictionary<string, string> ParseLine(string line)
        {
            string[] properties = line.Trim().Split();


            Dictionary<string, string> result = new Dictionary<string, string>();

            foreach(string prop in properties)
            {
                string[] kvp = prop.Split('=');
                result.Add(kvp.First(), kvp.Last());
            }

            return result;
        }
        static void Main(string[] args)
        {
            Dictionary<string, Dictionary<string, int>> registry = new Dictionary<string, Dictionary<string, int>>();

            for(;;)
            {
                string line = Console.ReadLine();
                if (line == "end") break;

                IReadOnlyDictionary<string, string> parsed = ParseLine(line);
                string username = parsed["user"];
                string ip = parsed["IP"];

                if(!registry.ContainsKey(username))
                {
                    registry[username] = new Dictionary<string, int>();
                }

                if(!registry[username].ContainsKey(ip))
                {
                    registry[username][ip] = 1;
                }
                else
                {
                    registry[username][ip]++;
                }
                
            }

            foreach(var user in registry.OrderBy(x=>x.Key))
            {
                Console.WriteLine($"{user.Key}:");

                Console.WriteLine(
                    string.Join(
                        ", ",
                        user.Value.Select(x => $"{x.Key} => {x.Value}")
                    )+"."
                );
            }
        }
    }
}
