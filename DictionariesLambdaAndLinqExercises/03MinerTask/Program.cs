﻿using System;
using System.Collections.Generic;

namespace _03MinerTask
{
    class Program
    {
        static void Main(string[] args)
        {
            var quantities=new Dictionary<string, int>();

            for(;;)
            {
                string res = Console.ReadLine();
                if (res == "stop") break;
                int quantity = int.Parse(Console.ReadLine());

                if (!quantities.ContainsKey(res))
                {
                    quantities[res] = quantity;
                }
                else
                {
                    quantities[res] += quantity;
                }
            }

            foreach(var x in quantities)
            {
                Console.WriteLine($"{x.Key} -> {x.Value}");
            }
        }
    }
}
