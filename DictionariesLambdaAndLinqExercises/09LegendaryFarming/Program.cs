﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _09LegendaryFarming
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> quantities = new Dictionary<string, int>();
            quantities["shards"] = quantities["motes"] = quantities["fragments"] = 0;

            for(;;)
            {
                string[] line = Console.ReadLine().Split();
                bool doBreak = false;
                for(int i=0;i<line.Length-1;i+=2)
                {
                    int quantity = int.Parse(line[i]);
                    string item = line[i + 1].ToLower();

                    int total;

                    if(!quantities.ContainsKey(item))
                    {
                        total = quantities[item] = quantity;
                    }
                    else
                    {
                        total = quantities[item] += quantity;
                    }

                    if (total >= 250)
                    {
                        string legendary = "";

                        if (item == "shards")
                        {
                            legendary = "Shadowmourne";
                        }
                        else if(item=="fragments")
                        {
                            legendary = "Valanyr";
                        }
                        else if(item=="motes")
                        {
                            legendary = "Dragonwrath";
                        }
                        else
                        {
                            legendary = null;
                        }

                        if (legendary!=null)
                        {
                            quantities[item] -= 250;
                            Console.WriteLine($"{legendary} obtained!");
                            doBreak = true;
                            break;
                        }
                    }   
                }
                if (doBreak)
                {
                    break;
                }
            }

            quantities
                    .Where(x => x.Key == "shards" || x.Key == "fragments" || x.Key == "motes")
                    .OrderByDescending(x => x.Value)
                    .ThenBy(x=>x.Key)
                    .ToList()
                    .ForEach(x => Console.WriteLine($"{x.Key}: {x.Value}"));

            quantities
                .Where(x => !(x.Key == "shards" || x.Key == "fragments" || x.Key == "motes"))
                .OrderBy(x => x.Key)
                .ToList()
                .ForEach(x => Console.WriteLine($"{x.Key}: {x.Value}"));
        }
    }
}
