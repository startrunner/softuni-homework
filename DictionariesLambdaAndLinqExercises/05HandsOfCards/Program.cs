﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _05HandsOfCards
{
    class Program
    {
        static readonly Dictionary<char, int> TypeValues = new Dictionary<char, int>(){
            { 'S', 4},
            { 'H', 3},
            { 'D', 2},
            { 'C', 1}
        };

        static readonly List<string> Powers = Enumerable.Concat(
            Enumerable.Range(0, 11).Select(x => x.ToString()),
            "JQKA".Select(x => x.ToString())
        )
        .ToList();

        static int GetValue(string card)
        {
            int type = TypeValues[card.Last()];
            int power = Powers.IndexOf(card.Substring(0, card.Length - 1));
            return type * power;
        }

        static void Main(string[] args)
        {
            var game = new Dictionary<string, HashSet<string>>();
            

            for (;;)
            {
                string str = Console.ReadLine();
                if (str=="JOKER") break;
                string[] line = str.Split(':');

                string name = line[0];
                string[] cards = line[1].Replace(" ", "").Split(',').Select(x => x.Trim()).ToArray();
                if(!game.ContainsKey(name))
                {
                    game[name] = new HashSet<string>();
                }
                foreach(var x in cards)
                {
                    game[name].Add(x);
                }
            }

            foreach(var player in game)
            {
                Console.WriteLine($"{player.Key}: {player.Value.Select(GetValue).Sum()}");

            }
        }
    }

}
/*
 
Pesho: 2C, 4H, 9H, AS, QS
Slav: 3H, 10S, JC, KD, 5S, 10S
Peshoslav: QH, QC, QS, QD
Slav: 6H, 7S, KC, KD, 5S, 10C
Peshoslav: QH, QC, JS, JD, JC
Pesho: JD, JD, JD, JD, JD, JD
JOKER

 */
