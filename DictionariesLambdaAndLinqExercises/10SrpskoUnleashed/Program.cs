﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _10SrpskoUnleashed
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, Dictionary<string, int>> registry = new Dictionary<string, Dictionary<string, int>>() ;

            for(;;)
            {
                string line = Console.ReadLine();
                if (line == "End") break;

                string[] split = line.Split('@');
                if (split.Count() != 2) continue;

                string artist = split.First();
                if (!artist.EndsWith(" ")) continue;
                artist = artist.Trim();

                split = split.Last().Split();

                if (split.Count() < 3) continue;

                int ticketPrice=0, ticketCount=0;

                if (!int.TryParse(split[split.Length - 2], out ticketPrice)) continue;
                if (!int.TryParse(split[split.Length - 1], out ticketCount)) continue;

                string venue = string.Join(" ", split.Take(split.Count() - 2));
                
                if(!registry.ContainsKey(venue))
                {
                    registry[venue] = new Dictionary<string, int>();
                }

                if (!registry[venue].ContainsKey(artist))
                {
                    registry[venue][artist] = ticketPrice * ticketCount;
                }
                else
                {
                    registry[venue][artist] += ticketPrice * ticketCount;
                }
            }

            foreach(var venue in registry)
            {
                Console.WriteLine(venue.Key);

                foreach(var artist in venue.Value.OrderByDescending(x=>x.Value))
                {
                    Console.WriteLine($"#  {artist.Key} -> {artist.Value}");
                }
            }
        }
    }
}
