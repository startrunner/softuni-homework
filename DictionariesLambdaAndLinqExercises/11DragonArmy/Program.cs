﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _11DragonArmy
{
    class Dragon
    {
        public string Name { get; set; }
        public int Damage { get; set; }
        public int Health { get; set; }
        public int Armor { get; set; }
        public string Color { get; set; }

        public override string ToString()
        {
            return $"{Name} -> damage: {Damage}, health: {Health}, armor: {Armor}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, List<Dragon>> registry = new Dictionary<string, List<Dragon>>();
            Dictionary<Tuple<string, string>, Dragon> allDragons = new Dictionary<Tuple<string, string>, Dragon>();
            int n = int.Parse(Console.ReadLine());

            for(int i = 0; i < n; i++)
            {
                string[] line = Console.ReadLine().Split();

                string color = line[0];
                string name = line[1];

                line = line.Skip(2).Select(x => x != "null" ? x : null).ToArray();
                int health = int.Parse(line[1] ?? "250");
                int damage = int.Parse(line[0] ?? "45");
                int armor = int.Parse(line[2] ?? "10");

                var dragon = new Dragon
                {
                    Name = name,
                    Armor = armor,
                    Damage = damage,
                    Health = health,
                    Color = color
                };

                allDragons[Tuple.Create(color, name)] = dragon;
            }

            foreach (var dragon in allDragons.Values) 
            {
                if(!registry.ContainsKey(dragon.Color))
                {
                    registry[dragon.Color] = new List<Dragon>();
                }

                registry[dragon.Color].Add(dragon);
            }

            foreach(var color in registry)
            {
                var dragons = color.Value.OrderBy(x=>x.Name).ToList();

                Console.WriteLine($"{color.Key}::({dragons.Average(x => x.Damage):0.00}/{dragons.Average(x => x.Health):0.00}/{dragons.Average(x => x.Armor):0.00})");
                dragons.ForEach(x => Console.WriteLine($"-{x}"));
            }
        }
    }
}
