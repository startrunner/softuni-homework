﻿using System;
using System.Collections.Generic;

namespace _01Phonebook
{
    class Program
    {
        static void Main(string[] args)
        {
            var phonebook = new Dictionary<string, string>();

            for(;;)
            {
                string[] line = Console.ReadLine().Split();

                if (line[0] == "END") break;


                if(line[0]=="A")
                {
                    phonebook[line[1]] = line[2];
                }
                else
                {
                    string name = line[1];
                    if(phonebook.ContainsKey(name))
                    {
                        Console.WriteLine($"{name} -> {phonebook[name]}");
                    }
                    else
                    {
                        Console.WriteLine($"Contact {name} does not exist.");
                    }
                }
            }
        }
    }
}
