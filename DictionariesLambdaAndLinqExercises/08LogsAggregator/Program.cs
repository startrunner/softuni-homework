﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _08LogsAggregator
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> duration = new Dictionary<string, int>();
            Dictionary<string, HashSet<string>> ips = new Dictionary<string, HashSet<string>>();

            int n = int.Parse(Console.ReadLine());

            for (int li = 0; li < n; li++)
            {
                string[] line = Console.ReadLine().Split();
                string ip = line[0];
                string user = line[1];
                int time = int.Parse(line[2]);

                if(!duration.ContainsKey(user))
                {
                    duration[user] = time;
                    ips[user] = new HashSet<string>(Enumerable.Repeat(ip, 1));
                }
                else
                {
                    duration[user] += time;
                    ips[user].Add(ip);
                }
            }

            foreach(var user in duration.OrderBy(x=>x.Key))
            {
                Console.WriteLine($"{user.Key}: {user.Value} [{string.Join(", ", ips[user.Key].OrderBy(x => x))}]");
            }
        }
    }
}
