﻿using System;
using System.Collections.Generic;

namespace _04FixEmails
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();

            for(;;)
            {
                string name = Console.ReadLine();
                if (name == "stop") break;

                string email = Console.ReadLine();

                string lower = email.ToLower();
                if (lower.EndsWith(".us") || lower.EndsWith(".uk")) continue;

                dict[name] = email;
            }

            foreach(var x in dict)
            {
                Console.WriteLine($"{x.Key} -> {x.Value}");
            }
        }
    }
}
