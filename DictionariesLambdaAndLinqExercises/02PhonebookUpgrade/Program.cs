﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02PhonebookUpgrade
{
    class Program
    {
        static void Main(string[] args)
        {
            var phonebook = new SortedDictionary<string, string>();

            for (;;)
            {
                string[] line = Console.ReadLine().Split();

                if (line[0] == "END") break;


                if(line[0]=="ListAll")
                {
                    foreach(var x in phonebook)
                    {
                        Console.WriteLine($"{x.Key} -> {x.Value}");
                    }
                }
                else if (line[0] == "A")
                {
                    phonebook[line[1]] = line[2];
                }
                else
                {
                    string name = line[1];
                    if (phonebook.ContainsKey(name))
                    {
                        Console.WriteLine($"{name} -> {phonebook[name]}");
                    }
                    else
                    {
                        Console.WriteLine($"Contact {name} does not exist.");
                    }
                }
            }
        }
    }
}
