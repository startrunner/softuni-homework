﻿using System;
using System.Linq;

namespace _01OrderByAge
{
    class Program
    {
        static void Main(string[] args)
        {
            Enumerable
                .Repeat(0, int.MaxValue)
                .Select(x => Console.ReadLine())
                .TakeWhile(x => x != "End")
                .Select(x =>  x.Split())
                .OrderBy(x => int.Parse(x[2]))
                .ToList()
                .ForEach(x => Console.WriteLine($"{x[0]} with ID: {x[1]} is {x[2]} years old."));
        }
    }
}
