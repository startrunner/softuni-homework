﻿using System;
using System.IO;
using System.Linq;

namespace _04PunctuationFinder
{
    class Program
    {
        static char[] PunctuationCharacters = ",.!?:".ToArray();
        static void Main(string[] args)
        {

            Console.WriteLine(
                string.Join(
                    "",
                    File
                        .ReadAllText("sample_text.txt")
                        .Where(x => !PunctuationCharacters.Contains(x)))
            );
                
        }
    }
}
