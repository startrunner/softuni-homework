﻿//using System;
//using System.Collections.Generic;
//using System.Linq;

//namespace _03Jarvis
//{
//    class Program
//    {
//        static void Main(string[] args)
//        {
//            Arm arm1=null, arm2=null;
//            Leg leg1=null, leg2=null;
//            Torso torso = null;
//            Head head = null;

//            ulong maxEnergyCapacity = ulong.Parse(Console.ReadLine());
//            for(;;)
//            {
//                string line = Console.ReadLine();

//                if (line == "Assemble!") break;

//                string[] split = line.Split(" ".ToArray(), StringSplitOptions.RemoveEmptyEntries);

//                string type = split[0];
//                ulong energyConsumption = ulong.Parse(split[1]);
                
//                if(type=="Head")
//                {
//                    int iq = int.Parse(split[2]);
//                    string skinMaterial = split[3];
//                    var newHead = new Head() {
//                        IQ = iq,
//                        SkinMaterial = skinMaterial,
//                        EnergyConsumption = energyConsumption
//                    };

//                    if(head==null || head.EnergyConsumption > newHead.EnergyConsumption)
//                    {
//                        head = newHead;
//                    }
//                }
//                else if(type=="Torso")
//                {
//                    double processorSize = double.Parse(split[2]);
//                    string housingMaterial = split[3];
//                    var newTorso = new Torso() {
//                        EnergyConsumption = energyConsumption,
//                        ProcessorSize = processorSize,
//                        HousingMaterial = housingMaterial,
//                    };

//                    if(torso==null || torso.EnergyConsumption > newTorso.EnergyConsumption)
//                    {
//                        torso = newTorso;
//                    }
//                }
//                else if(type=="Leg")
//                {
//                    int strenght = int.Parse(split[2]);
//                    int speed = int.Parse(split[3]);

//                    var leg = new Leg() {
//                        EnergyConsumption=energyConsumption,
//                        Strenght = strenght,
//                        Speed = speed
//                    };

//                    if (leg1 == null) leg1 = leg;
//                    else if (leg2 == null) leg2 = leg;
//                }
//                else if(type == "Arm")
//                {
//                    int reachDistance = int.Parse(split[2]);
//                    int fingerCount = int.Parse(split[3]);
//                    var arm = new Arm() {
//                        EnergyConsumption=energyConsumption,
//                        ReachDistance = reachDistance,
//                        FingerCount = fingerCount
//                    };

//                    if (arm1 == null) arm1 = arm;
//                    else if (arm2 == null) arm2 = arm;
//                    else
//                    {
//                        if(arm1.EnergyConsumption > arm.EnergyConsumption)
//                    }
//                }
//            }

//            if(head==null || torso==null || arm2==null || leg2==null)
//            {
//                Console.WriteLine("We need more parts!");
//            }
//            else
//            {
//                ulong energyConsumption =
//                    head.EnergyConsumption +
//                    torso.EnergyConsumption +
//                    arm1.EnergyConsumption +
//                    arm2.EnergyConsumption +
//                    leg1.EnergyConsumption +
//                    leg2.EnergyConsumption;

//                if(energyConsumption > maxEnergyCapacity)
//                {
//                    Console.WriteLine("We need more power!");
//                }
//                else
//                {
//                    Console.WriteLine("Jarvis:");
//                    Console.WriteLine("#Head:");
//                    Console.WriteLine($"###Energy consumption: {head.EnergyConsumption}");
//                    Console.WriteLine($"###IQ: {head.IQ}");
//                    Console.WriteLine($"###Skin material: {head.SkinMaterial}");
                    
//                    Console.WriteLine("#Torso:");
//                    Console.WriteLine($"###Energy consumption: {torso.EnergyConsumption}");
//                    Console.WriteLine($"###Processor size: {torso.ProcessorSize:F1}");
//                    Console.WriteLine($"###Corpus material: {torso.HousingMaterial}");

//                    foreach (var arm in new Arm[] { arm1, arm2 }.OrderBy(x => x.EnergyConsumption))
//                    {
//                        Console.WriteLine("#Arm:");
//                        Console.WriteLine($"###Energy consumption: {arm.EnergyConsumption}");
//                        Console.WriteLine($"###Reach: {arm.ReachDistance}");
//                        Console.WriteLine($"###Fingers: {arm.FingerCount}");
//                    }

//                    foreach (var leg in new Leg[] { leg1, leg2 }.OrderBy(x => x.EnergyConsumption))
//                    {
//                        Console.WriteLine("#Leg:");
//                        Console.WriteLine($"###Energy consumption: {leg.EnergyConsumption}");
//                        Console.WriteLine($"###Strength: {leg.Strenght}");
//                        Console.WriteLine($"###Speed: {leg.Strenght}");
//                    }

//                }
//            }
            
//        }
//    }

//    class Component
//    {
//        static int _idCount = 0;

//        public Component()
//        {

//        }
//        public int ID { get; } = _idCount++;
//        public ulong EnergyConsumption { get; set; }
//    }

//    internal class Head:Component
//    {
//        public int IQ { get; internal set; }
//        public string SkinMaterial { get; internal set; }
//    }

//    internal class Torso:Component
//    {
//        public string HousingMaterial { get; internal set; }
//        public double ProcessorSize { get; internal set; }
//    }

//    internal class Leg:Component
//    {
//        public int Strenght { get; internal set; }
//        public int Speed { get; internal set; }
//    }

//    internal class Arm:Component
//    {
//        public int ReachDistance { get; internal set; }
//        public int FingerCount { get; internal set; }
//    }
//}
