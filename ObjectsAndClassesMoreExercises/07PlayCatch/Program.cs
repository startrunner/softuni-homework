﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _07PlayCatch
{
    class Program
    {
        static readonly string IncorrectFormatMessage = "The variable is not in the correct format!";
        static readonly string IndexDoesNotExistMessage = "The index does not exist!";
        static void Main(string[] args)
        {
            int exc = 0;
            List<int> arr = Console.ReadLine().Split().Select(int.Parse).ToList();

            for(;;)
            {
                string[] split = null;
                try
                {
                    split = Console.ReadLine().Split();
                }
                catch
                {
                    break;
                }

                int index = 0, value = 0;
                int startIndex=0, endIndex=0;


                if(split.First()=="Replace")
                {
                    if(!int.TryParse(split[1], out index) ||!int.TryParse(split[2], out value))
                    {
                        Console.WriteLine(IncorrectFormatMessage);
                        exc++;
                    }
                    else
                    {
                        try
                        {
                            arr[index] = value;
                        }
                        catch
                        {
                            Console.WriteLine(IndexDoesNotExistMessage);
                            exc++;
                        }
                    }
                }
                else if(split.First() == "Print")
                {
                    if(!int.TryParse(split[1], out startIndex) || !int.TryParse(split[2], out endIndex))
                    {
                        Console.WriteLine(IncorrectFormatMessage);
                        exc++;
                    }
                    else
                    {
                        try
                        {
                            int a = arr[startIndex];
                            int b = arr[endIndex];

                            for(int i=startIndex;i<=endIndex;i++)
                            {
                                Console.Write(arr[i]);
                                if (i < endIndex) Console.Write(", ");
                                else Console.WriteLine();
                            }
                        }
                        catch
                        {
                            Console.WriteLine(IndexDoesNotExistMessage);
                            exc++;
                        }
                    }
                }
                else if(split.First()=="Show")
                {
                    if (!int.TryParse(split[1], out index))
                    {
                        Console.WriteLine(IncorrectFormatMessage);
                        exc++;
                    }
                    else
                    {
                        try
                        {
                            Console.WriteLine(arr[index]);
                        }
                        catch
                        {
                            Console.WriteLine(IndexDoesNotExistMessage);
                            exc++;
                        }
                    }

                }
                if (exc == 3) break;
            }
            Console.WriteLine(string.Join(", ", arr));
        }
    }
}
