﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _02VehicleCatalogue
{
    class Program
    {
        class Vehicle
        {
            public string Model { get; set; }
            public string Color { get; set; }
            public int Horsepower { get; set; }
        }
        static void Main(string[] args)
        {
            Dictionary<string, Vehicle>
                cars = new Dictionary<string, Vehicle>(),
                trucks = new Dictionary<string, Vehicle>();

            for (;;)
            {
                string line = Console.ReadLine();
                if (line == "End") break;

                string[] split = line.Split();

                var vehicle = new Vehicle() {
                    Model = split[1],
                    Color = split[2],
                    Horsepower = int.Parse(split[3])
                };

                if (split[0].ToLower() == "truck")
                {
                    trucks[vehicle.Model] = vehicle;
                }
                else
                {
                    cars[vehicle.Model] = vehicle;
                }
            }

            for (;;)
            {
                string line = Console.ReadLine();
                if (line == "Close the Catalogue") break;

                Vehicle vehicle = null;
                bool isTruck = true;

                if (cars.ContainsKey(line))
                {
                    vehicle = cars[line];
                    isTruck = false;
                }
                else
                {
                    vehicle = trucks[line];
                }

                Console.WriteLine($"Type: {(isTruck ? "Truck" : "Car")}");
                Console.WriteLine($"Model: {vehicle.Model}");
                Console.WriteLine($"Color: {vehicle.Color}");
                Console.WriteLine($"Horsepower: {vehicle.Horsepower}");
            }

            double carsAverage = 0;
            double trucksAverage = 0;

            if (cars.Any())
            {
                carsAverage = cars.Values.Select(x => x.Horsepower).Sum() / (double)cars.Count;
            }
            if (trucks.Any())
            {
                trucksAverage = trucks.Values.Select(x => x.Horsepower).Sum() / (double)trucks.Count;
            }

            Console.WriteLine($"Cars have average horsepower of: {carsAverage:F2}.");
            Console.WriteLine($"Trucks have average horsepower of: {trucksAverage:F2}.");
        }
    }
}
