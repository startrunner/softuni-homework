﻿using System;
using System.Linq;

namespace _02AdvertisementMessage
{
    class Program
    {
        static readonly string[]
            Phrases = new string[] { "Excellent product.", "Such a great product.", "I always use that product.", "Best product of its category.", "Exceptional product.", "I can’t live without this product." },
            Events = { "Now I feel good.", "I have succeeded with this product.", "Makes miracles. I am happy of the results!", "I cannot believe but now I feel awesome.", "Try it yourself, I am very satisfied.", "I feel great!" },
            Authors = { "Diana", "Petya", "Stella", "Elena", "Katya", "Iva", "Annie", "Eva" },
            Cities = { "Burgas", "Sofia", "Plovdiv", "Varna", "Ruse" };

        static void Main(string[] args)
        {
            Random rand = new Random();
            Enumerable
                .Repeat(0, int.Parse(Console.ReadLine()))
                .Select(x => $"{Phrases[rand.Next(0, Phrases.Length)]} {Events[rand.Next(0, Events.Length)]} {Authors[rand.Next(0, Authors.Length)]} {Cities[rand.Next(0, Cities.Length)]}")
                .ToList()
                .ForEach(Console.WriteLine);
        }
    }
}
