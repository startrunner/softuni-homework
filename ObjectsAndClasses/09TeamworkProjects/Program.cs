﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _09TeamworkProjects
{
    class Team
    {
        public string Name { get; set; }
        public HashSet<string> Members { get; } = new HashSet<string>();
        public string Creator { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var teams = new Dictionary<string, Team>();
            var teamOf = new Dictionary<string, Team>();
            var creators = new HashSet<string>();

            int n = int.Parse(Console.ReadLine());

            for(int i=0;i<n;i++)
            {
                string[] split = Console.ReadLine().Split('-');

                string username = split.First();
                string teamName = split.Last();

                if(teams.ContainsKey(teamName))
                {
                    Console.WriteLine($"Team {teamName} was already created!");
                }
                else if(teamOf.ContainsKey(username))
                {
                    Console.WriteLine($"{username} cannot create another team!");
                }
                else
                {
                    var team = new Team() {
                        Name = teamName,
                        Creator = username
                    };
                    team.Members.Add(username);

                    teams.Add(teamName, team);
                    teamOf[username] = team;
                    creators.Add(username);

                    Console.WriteLine($"Team {teamName} has been created by {username}!");
                }
            }

            for(;;)
            {
                string line = Console.ReadLine();
                if (line == "end of assignment") break;

                int arrow = line.IndexOf("->");

                string username = line.Substring(0, arrow);
                string teamName = line.Substring(arrow + 2);

                if(!teamOf.ContainsKey(username))
                {
                    teamOf.Add(username, null);
                }

                ;


                if(!teams.ContainsKey(teamName))
                {
                    Console.WriteLine($"Team {teamName} does not exist!");
                }
                else
                {
                    Team team = teams[teamName];
                    if(team.Members.Contains(username) && !creators.Contains(username))
                    {
                        Console.WriteLine($"Member {username} cannot join team {teamName}!");
                    }
                    else
                    {

                        Team oldTeam = teamOf[username];
                        oldTeam?.Members?.Remove(username);
                        team.Members.Add(username);
                        teamOf[username] = team;
                    }
                }
            }
            ;

            foreach (
                var team in teams
                    .Values
                    .Where(x=>x.Members.Count>1)
                    .OrderByDescending(x=>x.Members.Count)
                    .ThenBy(x=>x.Name)
            )
            {
                Console.WriteLine(team.Name);
                Console.WriteLine($"- {team.Creator}");
                foreach(string user in team.Members.Where(x=>x!=team.Creator))
                {
                    Console.WriteLine($"-- {user}");
                }
            }
            ;

            List<Team> emptyTeams = teams
                .Values
                .Where(x => x.Members.Count==1)
                .OrderBy(x=>x.Name)
                .ToList();

            Console.WriteLine("Teams to disband:");
            if(emptyTeams.Any())
            {
                emptyTeams.ForEach(x => Console.WriteLine(x.Name));
            }
        }
    }
}
