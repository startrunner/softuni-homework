﻿using System;
using System.Linq;

namespace _04AverageGrades
{
    class Student
    {
        double _averageGrade = -1;

        public string Name { get; set; }
        public double[] Grades { get; set; }

        public double AverageGrade =>
            _averageGrade != -1 ?
                _averageGrade :
                _averageGrade = Grades?.Average()??2.00;
    }

    class Program
    {

        static void Main(string[] args)
        {
            Student[] students = Enumerable
                .Repeat(0, int.Parse(Console.ReadLine()))
                .Select(x => Console.ReadLine().Split())
                .Select(x => new Student()
                {
                    Name = x.First(),
                    Grades = x.Skip(1).Select(double.Parse).ToArray()
                })
                .Where(x => x.AverageGrade >= 5)
                .ToArray();

            students
                .OrderBy(x => x.Name)
                .ThenByDescending(x => x.AverageGrade)
                .ToList()
                .ForEach(x=>Console.WriteLine($"{x.Name} -> {x.AverageGrade:F2}"));
        }
    }
}
