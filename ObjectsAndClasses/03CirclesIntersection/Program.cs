﻿using System;
using System.Linq;

namespace _03CirclesIntersection
{
    class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
    class Circle
    {
        public Point Center { get; set; }
        public int Radius { get; set; }

        public Circle(int[] args)
        {
            Center = new Point{
                X = args[0],
                Y = args[1]
            };

            Radius = args[2];
        }

        public bool IntersectsWith(Circle that)
        {
            double dist = Math.Sqrt(Math.Pow(Center.X - that.Center.X, 2) + Math.Pow(Center.Y - that.Center.Y, 2));

            if (Radius + that.Radius < dist) return false;
            else return true;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Circle a = new Circle(Console.ReadLine().Split().Select(int.Parse).ToArray());
            Circle b = new Circle(Console.ReadLine().Split().Select(int.Parse).ToArray());

            Console.WriteLine(a.IntersectsWith(b) ? "Yes" : "No");
        }
    }
}
