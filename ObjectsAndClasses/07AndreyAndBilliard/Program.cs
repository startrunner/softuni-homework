﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _07AndreyAndBilliard
{
    class Client
    {
        public string Name { get; set; }
        public Dictionary<string, int> Orders { get; } = new Dictionary<string, int>();
        public double TotalBill
        {
            get; set;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, double> products = new Dictionary<string, double>();
            Enumerable
                .Repeat(0, int.Parse(Console.ReadLine()))
                .Select(x => Console.ReadLine().Split('-'))
                .Select(x => Tuple.Create(x.First(), double.Parse(x.Last())))
                .ToList()
                .ForEach(x => products[x.Item1] = x.Item2);


            Dictionary<string, Client> clients = new Dictionary<string, Client>();

            for (;;)
            {
                string line = Console.ReadLine();
                if (line == "end of clients") break;

                string[] split = line.Split('-');
                string clientName = split.First();
                split = split.Last().Split(',');
                string product = split.First();
                int quantity = int.Parse(split.Last());

                if (!products.ContainsKey(product)) continue;

                Client client;
                if (!clients.ContainsKey(clientName))
                {
                    client = clients[clientName] = new Client()
                    {
                        Name = clientName
                    };
                }
                else
                {
                    client = clients[clientName];
                }

                client.TotalBill += products[product] * quantity;

                if (!client.Orders.ContainsKey(product))
                {
                    client.Orders[product] = quantity;
                }
                else
                {
                    client.Orders[product] += quantity;
                }

            }


            double total = 0;
            clients.Values
                .OrderBy(x => x.Name)
                .ToList()
                .ForEach(client =>
                {
                    Console.WriteLine(client.Name);
                    foreach (var orders in client.Orders) Console.WriteLine($"-- {orders.Key} - {orders.Value}");
                    Console.WriteLine($"Bill: {client.TotalBill:F2}");
                    total += client.TotalBill;
                });

            Console.WriteLine($"Total bill: {total:F2}");
        }
    }
}

