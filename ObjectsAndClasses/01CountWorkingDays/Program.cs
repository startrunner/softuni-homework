﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace _01CountWorkingDays
{
    class Program
    {

        static void Main(string[] args)
        {
            CultureInfo bg = CultureInfo.GetCultureInfo("BG-bg");

            DateTime from = DateTime.Parse(Console.ReadLine(), bg);
            DateTime to = DateTime.Parse(Console.ReadLine(), bg);

            List<int> holidays = new List<int>(){
                new DateTime(1, month:1, day:1).DayOfYear,
                new DateTime(1, month:3, day:3).DayOfYear,
                new DateTime(1, month:5, day:1).DayOfYear,
                new DateTime(1, month:5, day:6).DayOfYear,
                new DateTime(1, month:5, day:24).DayOfYear,
                new DateTime(1, month:9, day:6).DayOfYear,
                new DateTime(1, month:9, day:22).DayOfYear,
                new DateTime(1, month:11, day:1).DayOfYear,
                new DateTime(1, month:12, day:24).DayOfYear,
                new DateTime(1, month:12, day:25).DayOfYear,
                new DateTime(1, month:12, day:26).DayOfYear
            };

            int days = 0;

            for (DateTime i = from; i.Date <= to.Date; i += TimeSpan.FromDays(1))
            {
                if (!holidays.Contains(i.DayOfYear) && i.DayOfWeek != DayOfWeek.Sunday && i.DayOfWeek != DayOfWeek.Saturday)
                {
                    ;
                    days++;
                }
            }

            Console.WriteLine(days);

        }
    }
}
