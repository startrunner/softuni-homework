﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace _08MentorGroup
{
    class User
    {
        public string Username { get; set; }
        public List<DateTime> DatesAttended { get; } = new List<DateTime>();
        public List<string> Comments { get; } = new List<string>();
    }

    class Program
    {

        static void Main(string[] args)
        {
            CultureInfo bg = CultureInfo.GetCultureInfo("BG-bg");
            var users = new Dictionary<string, User>();

            for(;;)
            {
                string line = Console.ReadLine();
                if (line == "end of dates") break;

                string username = new string(line.TakeWhile(x => !char.IsWhiteSpace(x)).ToArray());

                User user;

                if(!users.ContainsKey(username))
                {
                    user = new User()
                    {
                        Username = username
                    };
                    users[username] = user;
                }
                else
                {
                    user = users[username];
                }

                if (username.Length != line.Length)
                {
                    string[] split = 
                    new string(
                        line.Skip(username.Length + 1).ToArray()
                    )
                    .Trim()
                    .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    user.DatesAttended.AddRange(
                        split.Select(x=> {
                            DateTime dt;
                            DateTime.TryParseExact(x, "dd/MM/yyyy", bg, DateTimeStyles.AllowInnerWhite, out dt);
                            return dt;
                        })
                    );
                }
            }

            for(;;)
            {
                string line = Console.ReadLine();
                if (line == "end of comments") break;

                try
                {
                    string username = new string(line.TakeWhile(x => x != '-').ToArray());
                    string comment = new string(line.Skip(username.Length + 1).ToArray());

                    if (users.ContainsKey(username))
                    {
                        users[username].Comments.Add(comment);
                    }
                }
                catch { continue; }
            }

            foreach(var userKvp in users.OrderBy(x=>x.Key))
            {
                var user = userKvp.Value;
                Console.WriteLine(user.Username);
                Console.WriteLine("Comments:");
                user.Comments.ForEach(x => Console.WriteLine($"- {x}"));
                Console.WriteLine("Dates attended:");
                foreach(var date in user.DatesAttended.OrderBy(x=>x.Date))
                {
                    Console.WriteLine($"-- {date:dd/MM/yyyy}");
                }
            }
        }
    }
}
