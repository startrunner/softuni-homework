﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _10StudentGroups
{
    class Student
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime RegistrationDate { get; set; }
    }
    class Town
    {
        IEnumerable<List<Student>> _studyGroups = null;
        public string Name { get; set; }
        public List<Student> Students { get; } = new List<Student>();
        public int Capacity { get; set; }


        public IEnumerable<List<Student>> GetStudyGroups() =>
            _studyGroups ?? (_studyGroups = GetStudyGroupsInternal());

        private IEnumerable<List<Student>> GetStudyGroupsInternal()
        {
            var lastGroup = new List<Student>();
            var students = new Queue<Student>();

            Students
                .OrderBy(x => x.RegistrationDate)
                .ThenBy(x => x.Name)
                .ThenBy(x => x.Email)
                .ToList()
                .ForEach(students.Enqueue);

            while(students.Any())
            {
                var x = students.Dequeue();

                if(lastGroup.Count==Capacity)
                {
                    yield return lastGroup;
                    lastGroup = new List<Student>();
                }

                lastGroup.Add(x);
            }

            if(lastGroup.Any())
            {
                yield return lastGroup;
            }
        }
    }
    class Program
    {
        static readonly IReadOnlyDictionary<string, int> MonthIndicies =
            new Dictionary<string, int>() {
                { "Jan", 1},
                { "Feb", 2},
                { "Mar", 3},
                { "Apr", 4},
                { "May", 5},
                { "Jun", 6},
                { "Jul", 7},
                { "Aug", 8},
                { "Sep", 9},
                { "Oct", 10},
                { "Nov", 11},
                { "Dec", 12}
            };
        static DateTime ParseDate(string str)
        {
            string[] split = str.Split('-');
            int day = int.Parse(split[0]);
            int month = MonthIndicies[split[1]];
            int year = int.Parse(split[2]);

            return new DateTime(year, month, day);
        }

        static void Main(string[] args)
        {
            List<Town> towns = new List<Town>();
            Town lastTown = null;
            for(;;)
            {
                string line = Console.ReadLine();
                if (line == "End") break;

                string[] split = line
                    .Split('|')
                    .Select(x => x.Trim())
                    .ToArray();

                if(split.Length==1)
                {
                    int arrPos = line.IndexOf("=>");
                    ;
                    string townName = line.Substring(0, arrPos).Trim();
                    int seatCount = int.Parse(
                        new string(
                            line
                                .Skip(arrPos + 2)
                                .SkipWhile(x => !char.IsDigit(x))
                                .TakeWhile(char.IsDigit)
                                .ToArray()
                        )
                        .Trim()
                    );

                    lastTown = new Town() {
                        Name = townName,
                        Capacity = seatCount
                    };
                    towns.Add(lastTown);
                }
                else
                {
                    string name = split[0];
                    string email = split[1];
                    DateTime regDate = ParseDate(split[2]);

                    var student = new Student() {
                        Name = name,
                        Email = email,
                        RegistrationDate = regDate
                    };

                    lastTown.Students.Add(student);
                }
            }

            int groupCount = towns
                .Select(x => x.GetStudyGroups().Count())
                .Sum();

            Console.WriteLine($"Created {groupCount} groups in {towns.Count} towns:");

            foreach(var town in towns.OrderBy(x=>x.Name))
            {
                foreach(var group in town.GetStudyGroups())
                {
                    Console.WriteLine($"{town.Name} => {string.Join(", ", group.Select(x => x.Email))}");
                }
            }
        }
    }
}
 