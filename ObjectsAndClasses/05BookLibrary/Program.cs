﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _05BookLibrary
{
    class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        public DateTime ReleaseDate { get; set; }
        public String Isbn { get; set; }
        public double Price { get; set; }
    }
    class Library
    {
        public string Name { get; set; }
        public List<Book> Books { get; set; }

        public IReadOnlyDictionary<string, double> GetStats()
        {
            Dictionary<string, double> total = new Dictionary<string, double>();

            foreach (var x in Books)
            {
                if (!total.ContainsKey(x.Author))
                {
                    total[x.Author] = x.Price;
                }
                else
                {
                    total[x.Author] += x.Price;
                }
            }

            return total;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            new Library()
            {
                Books =
                    Enumerable
                        .Repeat(1, int.Parse(Console.ReadLine()))
                        .Select(x => Console.ReadLine().Split())
                        .Select(x => new Book() { Author = x[1], Price = double.Parse(x.Last()) })
                        .ToList()
            }
            .GetStats()
            .OrderByDescending(x=>x.Value)
            .ThenBy(x=>x.Key)
            .ToList()
            .ForEach(x => Console.WriteLine($"{x.Key} -> {x.Value:F2}"));
        }
    }
}
