﻿using System;
using System.Text.RegularExpressions;
using System.Linq;

namespace _01ExtractEmails
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = Console.ReadLine();

            var matches = Regex.Matches(
                text,
                @"\b([a-z0-9]{1,}[\.\-_]){0,}[a-z0-9]{1,}\@(([a-z]{1,}\-){0,}[a-z]{1,}\.){1,}([a-z]{1,})\b",
                RegexOptions.IgnoreCase|
                RegexOptions.Multiline
            );

            foreach(var match in matches.OfType<Match>().Where(x=>x.Success))
            {
                ;
                Console.WriteLine(match);
            }
        }
    }
}
