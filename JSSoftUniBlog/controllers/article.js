const Article = require('mongoose').model('Article');

module.exports = {
    createGet: (req, res)=>{
        res.render('article/create');
    },
    createPost: (req, res)=>{
        let articleArgs = req.body;
        console.log("hai!");

        let errorMsg='';
        if(!req.isAuthenticated()){
            errorMsg='You should be logged in to make articles!';
        }
        else if(!articleArgs.title){
            errorMsg = 'Invalid Title!';
        }
        else if(!articleArgs.content){
            errorMsg = 'Invalid Content!';
        }

        if(errorMsg){
            res.render('article/create', {error: errorMsg});
            return;
        }

        articleArgs.author = req.user._id;
        Article.create(articleArgs).then(article=>{
           req.user.articles .push(article.id);
           req.user.save(err=>{
              if(err){
                  res.redirect('/', {error: err.message});
              }
              else{
                  res.redirect('/');
              }
           });
        });
    },
    details: (req, res)=>{
        let id = req.params.id;

        Article.findById(id).populate('author').then(article=>{
            res.render('article/details', article);
        });
    }
};



/*\
const User = require('mongoose').model('User');
const encryption = require('./../utilities/encryption');

module.exports = {
    registerGet: (req, res) => {
        res.render('user/register');
    },

    registerPost:(req, res) => {
        let registerArgs = req.body;

        User.findOne({email: registerArgs.email}).then(user => {
            let errorMsg = '';
            if (user) {
                errorMsg = 'User with the same username exists!';
            } else if (registerArgs.password !== registerArgs.repeatedPassword) {
                errorMsg = 'Passwords do not match!'
            }

            if (errorMsg) {
                registerArgs.error = errorMsg;
                res.render('user/register', registerArgs)
            } else {
                let salt = encryption.generateSalt();
                let passwordHash = encryption.hashPassword(registerArgs.password, salt);

                let userObject = {
                    email: registerArgs.email,
                    passwordHash: passwordHash,
                    fullName: registerArgs.fullName,
                    salt: salt
                };

                User.create(userObject).then(user => {
                    req.logIn(user, (err) => {
                        if (err) {
                            registerArgs.error = err.message;
                            res.render('user/register', registerArgs);
                            return;
                        }

                        res.redirect('/')
                    })
                })
            }
        })
    },

    loginGet: (req, res) => {
        res.render('user/login');
    },

    loginPost: (req, res) => {
        let loginArgs = req.body;
        User.findOne({email: loginArgs.email}).then(user => {
            if (!user ||!user.authenticate(loginArgs.password)) {
                let errorMsg = 'Either username or password is invalid!';
                loginArgs.error = errorMsg;
                res.render('user/login', loginArgs);
                return;
            }

            req.logIn(user, (err) => {
                if (err) {
                    console.log(err);
                    res.redirect('/user/login', {error: err.message});
                    return;
                }

                res.redirect('/');
            })
        })
    },

    logout: (req, res) => {
        req.logOut();
        res.redirect('/');
    }
};

* */