import  java.util.*;
public class Main {
    public  static  void main(String[] args){
        Scanner scanner=new Scanner(System.in);

        String[] split=scanner.nextLine().split(" ");
        int[] items=new int[split.length];
        for(int i=0;i<split.length;i++)items[i]=Integer.parseInt(split[i]);

        int maxStart=0, maxCount=1;

        for(int i=1, cCount=1;i<split.length;i++) {
            if(items[i]>items[i-1]) {
                cCount++;
                if(cCount>maxCount){
                    maxCount=cCount;
                    maxStart=i-cCount+1;
                }
            }
            else {
                cCount=1;
            }
        }

        for(int i=0;i<maxCount;i++) {
            System.out.print( items [i+maxStart]+" ");
        }
        System.out.println();
    }
}
