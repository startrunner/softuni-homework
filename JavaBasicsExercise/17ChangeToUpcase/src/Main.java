import  java.util.*;

public class Main {
    public  static  void main(String[] args){
        Scanner scanner=new Scanner(System.in);
        String input = scanner.nextLine();

        for(;;)
        {
            int from = input.indexOf("<upcase>");
            int to = input.indexOf("</upcase>");

            if(from==-1)break;

            StringBuilder builder=new StringBuilder();
            for(int i=0;i<input.length();i++){
                if(i>from && i<to+8){
                    builder.append(Character.toUpperCase(input.charAt(i)));
                }
                else{
                    builder.append(input.charAt(i));
                }
            }
            input=builder.toString();
        }

        System.out.println(input.replace("<UPCASE>", "").replace("</UPCASE>", ""));

    }
}
