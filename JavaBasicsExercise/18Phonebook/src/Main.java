import javafx.util.Pair;

import  java.util.*;

public class Main {
    public  static  void main(String[] args){
        Scanner scanner=new Scanner(System.in);
        Map<String, String> map=new TreeMap<>();

        for(;;){
            String line = scanner.nextLine();

            if(line.intern()=="END")break;
            else if(line.intern()=="ListAll"){
                for(String key : map.keySet()){
                    System.out.println(key + " -> " + map.get(key));
                }
                continue;
            }

            String[] split = line.split(" ");
            char command = split[0].charAt(0);
            String name = split[1];

            if(command=='A'){
                map.put(name, split[2]);
            }
            else if(command=='S'){
                if(!map.containsKey(name)){
                    System.out.println("Contact " + name + " does not exist.");
                }
                else{
                    System.out.println(name + " -> " + map.get(name));
                }
            }

        }
    }
}
