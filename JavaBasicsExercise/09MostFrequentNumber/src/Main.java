import  java.util.*;

public class Main {
    public  static  void main(String[] args){
        Scanner scanner=new Scanner(System.in);

        String[] split=scanner.nextLine().split(" ");
        int[] items=new int[split.length];
        for(int i=0;i<split.length;i++)items[i]=Integer.parseInt(split[i]);


        int[] countOf=new int[65536];

        int maxCount=0, maxItem=-1;

        for(int x : items) {
            countOf[x]++;
            if(countOf[x]>maxCount) {
                maxCount=countOf[x];
                maxItem=x;
            }
        }

        System.out.println(maxItem);
    }
}
