import java.util.Scanner;

public class Main {
    public  static  void main(String[] args){
        Scanner scanner=new Scanner(System.in);
        String email=scanner.nextLine();
        String text=scanner.nextLine();
        
        StringBuilder censorBuilder=new StringBuilder();
        for(int i=0, at=0;i<email.length();i++)
        {
            if(email.charAt(i)=='@'){
                at=1;
            }
            
            if(at==1){
                censorBuilder.append(email.charAt(i));
            }
            else{
                censorBuilder.append("*");
            }
        }
        
        String censor = censorBuilder.toString();
        
        System.out.println(text.replace(email, censor));
    }
}
