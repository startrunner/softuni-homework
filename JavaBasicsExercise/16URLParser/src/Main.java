import  java.util.*;

public class Main {
    public  static  void  main(String[] args){
        String server, protocol, resource;
        Scanner scan = new Scanner(System.in);
        String url = scan.nextLine();
        try {


            String[] split = url.split("://");

            if(split.length==1)throw new Exception();

            System.out.println("[protocol] = \"" + split[0] + "\"");
            split = split[1].split("/");
            System.out.println("[server] = \"" + split[0] + "\"");
            System.out.print("[resource] = \"" + String.join("/", Arrays.copyOfRange(split, 1, split.length)) + "\"");
        }
        catch(Exception ex){
            System.out.println("[protocol] = \"\"");
            System.out.println("[server] = \"" + url + "\"");
            System.out.println("[resource] = \"\"");
        }
    }
}
