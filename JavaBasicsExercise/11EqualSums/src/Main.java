import java.util.Scanner;

public class Main {
    public  static  void main(String[] args){
        Scanner scanner=new Scanner(System.in);

        String[] split=scanner.nextLine().split(" ");
        int[] items=new int[split.length];
        for(int i=0;i<split.length;i++)items[i]=Integer.parseInt(split[i]);

        int sumAll=0;
        for(int x : items)sumAll+=x;

        for(int sumLeft=0, i=0;i<items.length;i++) {
            int sumRight = sumAll - sumLeft - items[i];

            if(sumLeft==sumRight){
                System.out.println(i);
                return;
            }

            sumLeft+=items[i];
        }

        System.out.println("no");
    }
}
