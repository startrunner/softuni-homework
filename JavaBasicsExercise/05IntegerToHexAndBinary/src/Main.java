import  java.util.*;

public class Main {
    public  static  void main(String[] args){
        Scanner scan=new Scanner(System.in);

        int integer=scan.nextInt();

        System.out.println(Integer.toHexString(integer).toUpperCase());
        System.out.println(Integer.toBinaryString(integer));
    }
}
