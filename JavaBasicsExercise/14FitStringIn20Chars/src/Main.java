import  java.util.*;

public class Main {
    public  static  void main(String[] args){
        Scanner scanner=new Scanner(System.in);
        String str=scanner.nextLine();

        StringBuilder builder=new StringBuilder(str);
        while (builder.length()<20) {
            builder.append("*");
        }
        System.out.println(builder.toString().substring(0, 20));
    }
}
