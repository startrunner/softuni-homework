import java.util.Scanner;

public class Main {
    public  static  void main(String[] args){
        Scanner scanner=new Scanner(System.in);

        String[] split=scanner.nextLine().split(" ");
        int[] items=new int[split.length];
        for(int i=0;i<split.length;i++)items[i]=Integer.parseInt(split[i]);

        int bombNumber=scanner.nextInt();
        int power=scanner.nextInt();

        for(int i=0;i<items.length;i++) {
            if(items[i]==bombNumber){

                for(int j=i-power;j<=i+power;j++){
                    if(j>=0 && j<items.length){
                        items[j]=-Math.abs(items[j]);
                    }
                }
            }
        }

        int sum=0;
        for(int x : items) {
            if(x>=0)sum+=x;

            //System.out.print(x+ " ");
        }
        //System.out.println();

        System.out.println(sum);
    }
}
