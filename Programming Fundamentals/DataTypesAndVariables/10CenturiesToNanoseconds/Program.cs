﻿using System;
using System.Numerics;

namespace _10CenturiesToNanoseconds
{
    class Program
    {
        static void Main(string[] args)
        {
            ulong x=ulong.Parse(Console.ReadLine());
            Console.WriteLine($@"
{x} centuries =
{x *= 100} years =
{x = (ulong)(x * 365.2422)} days =
{x *= 24} hours =
{x *= 60} minutes =
{x *= 60} seconds =
{x *= 1000} milliseconds =
{x *= 1000} microseconds =
{new BigInteger(x) * 1000} nanoseconds".Replace("\n", " ").Replace("\r", "").Trim());
        }
    }
}
