﻿using System;
using System.Linq;

namespace _08EmployeeData
{
    class Program
    {
        static void Main(string[] args)
        {
            new string[]{
                "First name",
                "Last name",
                "Age",
                "Gender",
                "Personal ID",
                "Unique Employee number"
            }
            .Select(x => $"{x}: {Console.ReadLine()}")
            .ToList()
            .ForEach(x => Console.WriteLine(x));
        }
    }
}
