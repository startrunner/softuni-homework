﻿using System;
using System.Linq;

namespace _13VowelOrDigit
{
    class Program
    {
        static void Main(string[] args)
        {
            var vowels = new char[]{
                'a', 'e', 'o', 'i', 'u', 'y'
            };

            char c = Console.ReadLine().Single();
            if (char.IsDigit(c)) Console.WriteLine("digit");
            else if (vowels.Contains(c)) Console.WriteLine("vowel");
            else Console.WriteLine("other");

        }
    }
}
