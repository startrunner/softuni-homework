﻿using System;
using System.Linq;
using System.Text;

namespace _17PrintPartialASCII
{
    class Program
    {
        static void Main(string[] args)
        {
            byte a = byte.Parse(Console.ReadLine()), b = byte.Parse(Console.ReadLine());

            Console.WriteLine(string.Join(" ", Encoding.ASCII.GetChars(Enumerable.Range(a, b - a + 1).Select(x => (byte)x).ToArray())));
        }
    }
}
