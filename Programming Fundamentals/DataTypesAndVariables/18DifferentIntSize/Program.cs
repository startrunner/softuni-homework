﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _18DifferentIntSize
{
    class Program
    {
        static void Main(string[] args)
        {
            string num = Console.ReadLine();

            var types = new List<string>();
            sbyte a;
            byte b;
            short c;
            ushort d;
            int e;
            uint f;
            long g;

            if (sbyte.TryParse(num, out a)) types.Add("sbyte");
            if (byte.TryParse(num, out b)) types.Add("byte");
            if (short.TryParse(num, out c)) types.Add("short");
            if (ushort.TryParse(num, out d)) types.Add("ushort");
            if (int.TryParse(num, out e)) types.Add("int");
            if (uint.TryParse(num, out f)) types.Add("uint");
            if (long.TryParse(num, out g)) types.Add("long");

            if (types.Any())
            {
                Console.WriteLine($"{num} can fit in:");
                types
                    .Select(x => $"* {x}")
                    .ToList()
                    .ForEach(x => Console.WriteLine(x));
            }
            else
            {
                Console.WriteLine($"{num} can't fit in any type");
            }
            
        }
    }
}
