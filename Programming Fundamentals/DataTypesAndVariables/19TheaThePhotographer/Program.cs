﻿using System;

namespace _19TheaThePhotographer
{
    class Program
    {
        static void Main(string[] args)
        {
            ulong n = ulong.Parse(Console.ReadLine());
            ulong filterTime = ulong.Parse(Console.ReadLine());
            ulong filterFactor = ulong.Parse(Console.ReadLine());
            ulong uploadTime = ulong.Parse(Console.ReadLine());

            ulong totalFilterTime = n * filterTime;
            ulong totalUploadTime = ((n * filterFactor / 100) + (n * filterFactor % 100 == 0 ? 0UL : 1UL)) * uploadTime;
            ulong totalTime = totalFilterTime + totalUploadTime;

            var time = TimeSpan.FromSeconds(totalTime);

            Console.WriteLine($"{time.Days}:{time.Hours:00}:{time.Minutes:00}:{time.Seconds:00}");
        }
    }
}
