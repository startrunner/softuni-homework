﻿using System;

namespace _05BoolVariable
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Convert.ToBoolean(Console.ReadLine()) ? "Yes" : "No");
        }
    }
}
