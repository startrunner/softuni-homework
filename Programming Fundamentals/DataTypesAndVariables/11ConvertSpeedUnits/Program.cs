﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11ConvertSpeedUnits
{
    class Program
    {
        static void Main(string[] args)
        {
            float dist = float.Parse(Console.ReadLine());
            TimeSpan time = new TimeSpan(
                int.Parse(Console.ReadLine()),
                int.Parse(Console.ReadLine()),
                int.Parse(Console.ReadLine()));

            float totalSeconds = (float)time.TotalSeconds;
            float totalHours = (float)time.TotalHours;

            //s=v*t
            //distance = speed*t
            //speed = distance/t

            float speedMS = dist / totalSeconds;
            float speedKmH = (dist / 1000) / totalHours;
            float speedMH = (dist / 1609) / totalHours;

            Console.WriteLine(speedMS);
            Console.WriteLine(speedKmH);
            Console.WriteLine(speedMH);
        }
    }
}
