﻿using System;

namespace _14IntToHexAndBin
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = int.Parse(Console.ReadLine());
            Console.WriteLine(Convert.ToString(x, 16).ToUpper());
            Console.WriteLine(Convert.ToString(x, 2));
        }
    }
}
