﻿using System;

namespace _07ExchangeVarValues
{
    class Program
    {
        static void Main(string[] args)
        {
            string a = Console.ReadLine(), b = Console.ReadLine();
            Console.WriteLine("Before:");
            Console.WriteLine($"a = {a}");
            Console.WriteLine($"b = {b}");
            Console.WriteLine("After:");
            Console.WriteLine($"a = {b}");
            Console.WriteLine($"b = {a}");
        }
    }
}
