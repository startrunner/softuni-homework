﻿using System;
using System.Linq;

namespace _09ReversedChars
{
    class Program
    {
        static void Main(string[] args)
        {
            Enumerable.
                Repeat(0, 3)
                .Select(x => Console.ReadLine().First())
                .Reverse()
                .ToList()
                .ForEach(x => Console.Write(x));
            Console.WriteLine();
        }
    }
}
