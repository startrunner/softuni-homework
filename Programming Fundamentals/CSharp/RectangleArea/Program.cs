﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RectangleArea
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine((Enumerable.Repeat(0, 2).Select(x => double.Parse(Console.ReadLine())).Aggregate(1.0, (x, y) => x * y)).ToString("0.00"));
        }
    }
}
