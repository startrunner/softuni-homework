﻿using System;
using System.Linq;

namespace DebitCardNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(string.Join(" ", Enumerable.Repeat(0, 4).Select(x => int.Parse(Console.ReadLine()).ToString("0000"))));
        }
    }
}
