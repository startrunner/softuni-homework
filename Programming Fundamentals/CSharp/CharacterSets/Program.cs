﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterSets
{
    class Program
    {
        static void Main(string[] args)
        {
            string name = Console.ReadLine();
            int health = int.Parse(Console.ReadLine());
            int maxHealth = int.Parse(Console.ReadLine());
            int energy = int.Parse(Console.ReadLine());
            int maxEnergy = int.Parse(Console.ReadLine());

            Console.WriteLine($"Name: {name}");
            Console.WriteLine($"Health: |{string.Join("", Enumerable.Repeat('|', health))}{string.Join("", Enumerable.Repeat(".", maxHealth-health))}|");
            Console.WriteLine($"Energy: |{string.Join("", Enumerable.Repeat('|', energy))}{string.Join("", Enumerable.Repeat(".", maxEnergy - energy))}|");
        }
    }
}
