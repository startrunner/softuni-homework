﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace _04GrabAndGo
{
    class Program
    {
        static void Main(string[] args)
        {
            BigInteger cSum = 0, sumb4 = 0;
            var arr = Console.ReadLine().Split().Select(BigInteger.Parse).ToArray();
            BigInteger k = BigInteger.Parse(Console.ReadLine());
            bool any = false;

            for(int i=0;i<arr.Length;i++)
            {
                if(arr[i]==k)
                {
                    any = true;
                    sumb4 = cSum;
                }
                cSum += arr[i];
            }

            if(!any)
            {
                Console.WriteLine($"No occurrences were found!");
            }
            else
            {
                Console.WriteLine(sumb4);
            }
        }
    }
}
