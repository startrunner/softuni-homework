﻿using System;
using System.Linq;

namespace _07InventoryMatcher
{
    class Program
    {
        static void Main(string[] args)
        {
            var stuff = Enumerable
                .Zip(
                Enumerable.Zip(
                    Console.ReadLine().Split(),
                    Console.ReadLine().Split().Select(long.Parse),
                    (x, y) => Tuple.Create(x, y)
                ),
                Console.ReadLine().Split().Select(decimal.Parse),
                (x, y) => Tuple.Create(x.Item1, x.Item2, y)
            )
            .ToArray();

            var indexOf = stuff
                .Select((x, i) => Tuple.Create(x.Item1, i))
                .ToDictionary(x => x.Item1, x => x.Item2);

            for(;;)
            {
                string product = Console.ReadLine();
                if (product == "done") break;

                int i = indexOf[product];
                decimal price = stuff[i].Item3;
                long quantity = stuff[i].Item2;

                Console.WriteLine($"{product} costs: {price}; Available quantity: {quantity}");

            }
        }
    }
}
