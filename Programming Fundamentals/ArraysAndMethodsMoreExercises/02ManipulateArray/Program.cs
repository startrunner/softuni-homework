﻿using System;
using System.Linq;

namespace _02ManipulateArray
{
    class Program
    {
        static void Main(string[] args)
        {
            var arr = Console.ReadLine().Split(new char[0], StringSplitOptions.RemoveEmptyEntries).Select(x => x);

            int n = int.Parse(Console.ReadLine());
            for(int i=0;i<n;i++)
            {
                string[] command = Console.ReadLine().Split();
                switch(command[0])
                {
                    case "Distinct":
                        arr = arr.Distinct();
                        break;
                    case "Reverse":
                        arr = arr.Reverse();
                        break;
                    case "Replace":
                        int x = int.Parse(command[1]);
                        string[] newArr = arr.ToArray();
                        newArr[x] = command[2];
                        arr = newArr;
                        break;
                }
            }

            Console.WriteLine(string.Join(", ", arr));

        }
    }
}
