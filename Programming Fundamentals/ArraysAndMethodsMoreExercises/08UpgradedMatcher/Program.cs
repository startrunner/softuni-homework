﻿using System;
using System.Linq;

namespace _08UpgradedMatcher
{
    class Program
    {
        static void Main(string[] args)
        {
            var stuff = Enumerable
                .Zip(
                Enumerable.Zip(
                    Console.ReadLine().Split(),
                    Enumerable.Concat(Console.ReadLine().Split().Select(long.Parse), Enumerable.Repeat(0L, int.MaxValue)),
                    (x, y) => Tuple.Create(x, y)
                ),
                Console.ReadLine().Split().Select(decimal.Parse),
                (x, y) => Tuple.Create(x.Item1, x.Item2, y)
            )
            .ToArray();

            var indexOf = stuff
                .Select((x, i) => Tuple.Create(x.Item1, i))
                .ToDictionary(x => x.Item1.ToLower(), x => x.Item2);

            for (;;)
            {
                string line = Console.ReadLine();
                if (line == "done") break;

                string[] lineSplit = line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                string product = lineSplit[0];
                long quantity = long.Parse(lineSplit[1]);

                int i = indexOf[product.ToLower()];
                decimal price = stuff[i].Item3;

                if (quantity <= stuff[i].Item2)
                {
                    Console.WriteLine($"{product} x {quantity} costs {stuff[i].Item3 * quantity:0.00}");
                    stuff[i] = Tuple.Create(stuff[i].Item1, stuff[i].Item2 - quantity, stuff[i].Item3);
                }
                else
                {
                    Console.WriteLine($"We do not have enough {product}");
                }

            }
        }
    }
}
