﻿using System;
using System.Linq;
using System.Numerics;

namespace _06Heists
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] in1 = Console.ReadLine().Split().Select(int.Parse).ToArray();
            int jewelPrice = in1[0], goldPrice = in1[1];

            BigInteger totalEarnings = 0;
            BigInteger totalExpenses = 0;

            for(;;)
            {
                string line = Console.ReadLine();
                if (line == "Jail Time") break;

                string[] lineSplit = line.Split();
                string loot = lineSplit[0];
                foreach(char x in loot)
                {
                    if (x == '%') totalEarnings += jewelPrice;
                    if (x == '$') totalEarnings += goldPrice;
                }
                int expenses = int.Parse(lineSplit[1]);

                totalExpenses += expenses;
            }

            if(totalExpenses<=totalEarnings)
            {
                Console.WriteLine($"Heists will continue. Total earnings: {totalEarnings - totalExpenses}.");
            }
            else
            {
                Console.WriteLine($"Have to find another job. Lost: {totalExpenses - totalEarnings}.");
            }
        }
    }
}
