﻿using System;
using System.Linq;

namespace _03SafeManipulation
{
    class Program
    {
        static void Main(string[] args)
        {
            var arr = Console.ReadLine().Split(new char[0], StringSplitOptions.RemoveEmptyEntries).Select(x => x);

            for(;;)
            {
                bool end = false;
                string[] command = Console.ReadLine().Split();
                switch (command[0])
                {
                    case "Distinct":
                        arr = arr.Distinct();
                        break;
                    case "Reverse":
                        arr = arr.Reverse();
                        break;
                    case "Replace":
                        try
                        {
                            uint x = uint.Parse(command[1]);
                            string[] newArr = arr.ToArray();
                            newArr[x] = command[2];
                            arr = newArr;
                        }
                        catch
                        {
                            Console.WriteLine("Invalid input!");
                        }
                            break;
                    case "END":
                        end = true;
                        break;
                    default:
                        Console.WriteLine("Invalid input!");
                        break;
                }
                if (end) break;
            }

            Console.WriteLine(string.Join(", ", arr));

        }
    }
}
