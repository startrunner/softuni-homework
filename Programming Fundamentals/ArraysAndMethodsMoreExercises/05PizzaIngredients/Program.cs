﻿using System;
using System.Linq;
using System.Text;

namespace _05PizzaIngredients
{
    class Program
    {
        static void Main(string[] args)
        {
            StringBuilder all = new StringBuilder();
            int len=0;

            for(;;)
            {
                string s = Console.ReadLine();
                if (int.TryParse(s, out len)) break;
                all.Append(s);
                all.Append(' ');
            }

            var ingredients = all
                .ToString()
                .Split(new char[0], StringSplitOptions.RemoveEmptyEntries).Where(x => x.Length == len)
                .Take(10)
                .ToList();

            ingredients.ForEach(x => Console.WriteLine($"Adding {x}."));

            Console.WriteLine($"Made pizza with total of {ingredients.Count} ingredients.");
            Console.WriteLine($"The ingredients are: {string.Join(", ", ingredients)}.");
        }
    }
}
