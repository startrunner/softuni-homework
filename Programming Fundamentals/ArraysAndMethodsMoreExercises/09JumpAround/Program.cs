﻿using System;
using System.Linq;

namespace _09JumpAround
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;
            int[] arr = Console.ReadLine().Split().Select(int.Parse).ToArray();

            for(int i=0; ;)
            {
                int x = arr[i];
                sum += x;

                if (i + x < arr.Length) i += x;
                else if (i - x > -1) i -= x;
                else break;
            }

            Console.WriteLine(sum);
        }
    }
}
