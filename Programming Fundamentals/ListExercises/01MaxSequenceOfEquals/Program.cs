﻿using System;
using System.Linq;

namespace _01MaxSequenceOfEquals
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = Console
                .ReadLine()
                .Split()
                .Select(int.Parse)
                .ToList();

            int item = 0, maxCount = 0;

            for(int cItem=0, cCount=0, i=0;i<list.Count;i++)
            {
                if(i!=0 && list[i]==list[i-1])
                {
                    cCount++;
                }
                else
                {
                    cCount = 1;
                    cItem = list[i];
                }

                if(cCount>maxCount)
                {
                    maxCount = cCount;
                    item = cItem;
                }
            }

            Console.WriteLine(string.Join(" ", Enumerable.Repeat(item, maxCount)));
        }
    }
}
