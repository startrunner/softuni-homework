﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _05ArrayManipulator
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = Console
                .ReadLine()
                .Split()
                .Select(int.Parse)
                .ToList();

            for (;;)
            {
                string[] line = Console.ReadLine().Split();

                bool done = false;
                int index = 0, item = 0, positions = 0;

                switch (line.First())
                {
                    case "add":
                        index = int.Parse(line[1]);
                        item = int.Parse(line[2]);
                        list.Insert(index, item);
                        break;
                    case "addMany":
                        index = int.Parse(line[1]);
                        list.InsertRange(index, line.Skip(2).Select(int.Parse));
                        break;
                    case "contains":
                        item = int.Parse(line[1]);
                        Console.WriteLine(list.IndexOf(item));
                        break;
                    case "remove":
                        index = int.Parse(line[1]);
                        list.RemoveAt(index);
                        break;
                    case "shift":
                        positions = int.Parse(line[1]);
                        var shifted = new List<int>();
                        for (int i = 0; i < list.Count; i++)
                        {
                            shifted.Add(list[(i + positions) % list.Count]);
                        }
                        list = shifted;
                        break;
                    case "sumPairs":
                        var summed = new List<int>();

                        for (int i = 0; i < list.Count; i += 2)
                        {
                            if (i + 1 < list.Count)
                            {
                                summed.Add(list[i] + list[i + 1]);
                            }
                            else
                            {
                                summed.Add(list[i]);
                            }
                        }

                        list = summed;
                        break;
                    default:
                        done = true;
                        break;
                }

                if(done)
                {
                    Console.WriteLine($"[{string.Join(", ", list)}]");
                    break;
                }
            }

        }
    }
}
