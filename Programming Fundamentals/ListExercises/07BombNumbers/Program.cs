﻿using System;
using System.Linq;

namespace _07BombNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = Console
                .ReadLine()
                .Split()
                .Select(int.Parse)
                .ToList();

            bool[] bombed = Enumerable.Repeat(false, list.Count).ToArray();

            string[] line2 = Console.ReadLine().Split();
            int bomb = int.Parse(line2[0]);
            int power = int.Parse(line2[1]);
            int sum = 0;

            for(int bombIndex=list.IndexOf(bomb);bombIndex!=-1;bombIndex=list.IndexOf(bomb, bombIndex + 1))
            {
                if (bombed[bombIndex]) continue;
                bombed[bombIndex] = true;
                sum += list[bombIndex];

                for (int i = Math.Max(bombIndex - power, 0); i < bombIndex; i++)
                {
                    if(!bombed[i])
                    {
                        bombed[i] = true;
                        sum += list[i];
                    }
                }

                for (int i = Math.Min(bombIndex + power, list.Count - 1); i > bombIndex; i--)
                {
                    if (!bombed[i])
                    {
                        bombed[i] = true;
                        sum += list[i];
                    }
                }
            }

            Console.WriteLine(list.Sum()-sum);
        }
    }
}
