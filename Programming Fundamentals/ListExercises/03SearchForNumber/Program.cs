﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _03SearchForNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list = Console
                .ReadLine()
                .Split()
                .Select(int.Parse)
                .ToList();

            int[] query = Console
                .ReadLine()
                .Split()
                .Select(int.Parse)
                .ToArray();

            int take = query[0], skip = query[1], item = query[2];

            if(list.Take(take).Skip(skip).Contains(item))
            {
                Console.WriteLine("YES!");
            }
            else
            {
                Console.WriteLine("NO!");
            }
        }
    }
}
