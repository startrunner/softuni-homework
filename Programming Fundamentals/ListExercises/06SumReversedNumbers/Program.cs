﻿using System;
using System.Linq;

namespace _06SumReversedNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(
                Console
                    .ReadLine()
                    .Split()
                    .Select(x => new string(x.Reverse().ToArray()))
                    .Select(int.Parse)
                    .Sum()
            );
        }
    }
}
