﻿using System;
using System.Linq;

namespace _02ChangeList
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = Console
                .ReadLine()
                .Split()
                .Select(int.Parse)
                .ToList();

            for(;;)
            {
                bool done = false;
                bool odd = true;
                string[] line = Console.ReadLine().Split().ToArray();

                int element=0, position=0;

                switch(line.First())
                {
                    case "Insert":
                        element = int.Parse(line[1]);
                        position = int.Parse(line[2]);
                        list.Insert(position, element);
                        break;
                    case "Delete":
                        element = int.Parse(line[1]);
                        list.RemoveAll(x => x == element);
                        break;
                    case "Even":
                        done = true;
                        odd = false;
                        break;
                    case "Odd":
                        odd = true;
                        done = true;
                        break;
                }
                if (done)
                {
                    Console.WriteLine(string.Join(" ", list.Where(x => (x % 2 == 1) == odd)));
                    break;
                }
            }


        }
    }
}
