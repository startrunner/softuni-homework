﻿using System;
using System.Linq;

namespace _09MakeAWord
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(
                "The word is: " +
                new string(
                    Enumerable.Repeat(0, int.Parse(Console.ReadLine()))
                    .Select(x => Console.ReadLine().First())
                    .ToArray()
                )
            );
        }
    }
}
