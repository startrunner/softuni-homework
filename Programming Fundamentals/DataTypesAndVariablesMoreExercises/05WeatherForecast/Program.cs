﻿using System;

namespace _05WeatherForecast
{
    class Program
    {
        static void Main(string[] args)
        {
            string x = Console.ReadLine();

            sbyte a;
            int b;
            long c;
            //float d;

            if (sbyte.TryParse(x, out a)) Console.WriteLine("Sunny");
            else if (int.TryParse(x, out b)) Console.WriteLine("Cloudy");
            else if (long.TryParse(x, out c)) Console.WriteLine("Windy");
            else Console.WriteLine("Rainy");
        }
    }
}
