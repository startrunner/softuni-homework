﻿using System;
using System.Linq;
using System.Numerics;

namespace _06CatchTheThief
{
    class Program
    {
        static void Main(string[] args)
        {
            BigInteger max;
            switch(Console.ReadLine())
            {
                case "sbyte":
                    max = sbyte.MaxValue;
                    break;
                case "int":
                    max = int.MaxValue;
                    break;
                case "long":
                    max = long.MaxValue;
                    break;
                default:
                    max = 0;
                    break;
            }

            Console.WriteLine(
                Enumerable
                    .Repeat(0, int.Parse(Console.ReadLine()))
                    .Select(x => BigInteger.Parse(Console.ReadLine()))
                    .OrderBy(x => x - max < 0 ? max - x : x - max)
                    .First()
            );
        }
    }
}
