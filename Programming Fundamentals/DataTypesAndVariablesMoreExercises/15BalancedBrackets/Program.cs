﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _15BalancedBrackets
{
    class Program
    {
        static void Main(string[] args)
        {
            var stack = new List<char>();
            int n = int.Parse(Console.ReadLine());
            for(int i=0;i<n;i++)
            {
                foreach(char c in Console.ReadLine())
                {
                    if(c=='(' || c==')')
                    {
                        if(stack.LastOrDefault()=='(' && c==')')
                        {
                            stack.RemoveAt(stack.Count - 1);
                        }
                        else
                        {
                            stack.Add(c);
                        }
                    }
                }
            }
            if(stack.Any())
            {
                Console.WriteLine("UNBALANCED");
            }
            else
            {
                Console.WriteLine("BALANCED");
            }
        }
    }
}
