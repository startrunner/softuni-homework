﻿using System;
using System.Linq;

namespace _12BeerKegs
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(
                Enumerable
                    .Repeat(0, int.Parse(Console.ReadLine()))
                    .Select(
                        x =>
                            new Tuple<string, double, double>(
                                Console.ReadLine(),
                                double.Parse(Console.ReadLine()),
                                double.Parse(Console.ReadLine())
                            )
                    )
                    .OrderByDescending(x => Math.PI * x.Item2 * x.Item2 * x.Item3)
                    .First()
                    .Item1
            );
        }
    }
}
