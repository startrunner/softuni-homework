﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14BoatSimulator
{
    class Program
    {
        static void Main(string[] args)
        {
            int posA = 0, posB = 0;
            char a = Console.ReadLine().First(), b = Console.ReadLine().First();
            int n = int.Parse(Console.ReadLine());

            for(int i=1;i<=n;i++)
            {
                string x = Console.ReadLine();
                if(x=="UPGRADE")
                {
                    a += (char)3;
                    b += (char)3;
                }
                else if(i%2==1)
                {
                    posA += x.Length;
                }
                else
                {
                    posB += x.Length;
                }

                if (posA >= 50)
                {
                    Console.WriteLine(a);
                    return;
                }
                if(posB>=50)
                {
                    Console.WriteLine(b);
                    return;
                }   
            }

            if (posA > posB) Console.WriteLine(a);
            else Console.WriteLine(b);
        }
    }
}
