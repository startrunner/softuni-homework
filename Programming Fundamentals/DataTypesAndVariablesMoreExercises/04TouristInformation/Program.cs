﻿using System;
using System.Linq;

namespace _04TouristInformation
{
    class Program
    {
        static void Main(string[] args)
        {
            Enumerable
                .Repeat(
                    new Tuple<string, double>(Console.ReadLine(), double.Parse(Console.ReadLine())),
                    1
                )
                .Select(
                    x =>
                        x.Item1 == "miles" ? $"{x.Item2} miles = {x.Item2 * 1.6:0.00} kilometers" :
                        x.Item1 == "inches" ? $"{x.Item2} inches = {x.Item2 * 2.54:0.00} centimeters" :
                        x.Item1 == "feet" ? $"{x.Item2} feet = {x.Item2 * 30:0.00} centimeters" :
                        x.Item1 == "yards" ? $"{x.Item2} yards = {x.Item2 * 0.91:0.00} meters" :
                        x.Item1 == "gallons" ? $"{x.Item2} gallons = {x.Item2 * 3.8:0.00} liters" :
                        "Unknown Unit"
                )
                .ToList()
                .ForEach(x => Console.WriteLine(x));
        }
    }
}
