﻿using System;
using System.Linq;

namespace _08HouseBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            Enumerable
                .Repeat(0, 1)
                .Select(x => new Tuple<long, long>(long.Parse(Console.ReadLine()), long.Parse(Console.ReadLine())))
                .Select(x => Math.Abs(x.Item1) > Math.Abs(x.Item2) ? new Tuple<long, long>(x.Item2, x.Item1) : x)
                .Select(x => new Tuple<long, long>(x.Item1, x.Item2))
                .Select(x => x.Item1 * 4 + x.Item2 * 10)
                .ToList()
                .ForEach(x => Console.WriteLine(x));
        }
    }
}
