﻿using System;
using System.Linq;
using System.Numerics;

namespace _07SentenceTheThief
{
    class Program
    {
        static void Main(string[] args)
        {
            BigInteger max;
            switch (Console.ReadLine())
            {
                case "sbyte":
                    max = sbyte.MaxValue;
                    break;
                case "int":
                    max = int.MaxValue;
                    break;
                case "long":
                    max = long.MaxValue;
                    break;
                default:
                    max = 0;
                    break;
            }

            BigInteger thief = Enumerable
                .Repeat(0, int.Parse(Console.ReadLine()))
                .Select(x => BigInteger.Parse(Console.ReadLine()))
                .OrderBy(x => x - max < 0 ? max - x : x - max)
                .First();

            BigInteger sbm = thief > 0 ? sbyte.MaxValue : sbyte.MinValue;
            BigInteger sentence = (thief / sbm + (thief % sbm != 0 ? 1 : 0));

            //Console.WriteLine($"thief: {thief}");

            Console.WriteLine($"Prisoner with id {thief} is sentenced to {sentence} year{(sentence != 1 ? "s" : "")}");
        }
    }
}
