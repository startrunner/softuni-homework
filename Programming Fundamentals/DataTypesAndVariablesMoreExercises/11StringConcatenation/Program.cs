﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11StringConcatenation
{
    class Program
    {
        static void Main(string[] args)
        {
            Enumerable.Repeat(
                new Tuple<string, string>(
                    Console.ReadLine(),
                    Console.ReadLine()
                ),
                1
            )
            .Select(
                tuple =>
                    string.Join(
                        tuple.Item1,
                        Enumerable
                            .Range(1, int.Parse(Console.ReadLine()))
                            .Select(x => new Tuple<int, string>(x, Console.ReadLine()))
                            .Where(x => (x.Item1 % 2 == 0 ? "even" : "odd") == tuple.Item2)
                            .Select(x => x.Item2)
                    )
            )
            .ToList()
            .ForEach(x => Console.WriteLine(x));
        }
    }
}
