﻿using System;
using System.Linq;
using System.Text;

namespace _03WaterOverflow
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(
                Enumerable
                    .Repeat(0, int.Parse(Console.ReadLine()))
                    .Select(x => int.Parse(Console.ReadLine()))
                    .Concat(Enumerable.Repeat(-1, 1))
                    .Aggregate(
                        new Tuple<StringBuilder, int>(new StringBuilder(), 0),
                        (t, x) =>
                            x != -1 ? (
                                t.Item2 + x <= 255 ?
                                new Tuple<StringBuilder, int>(t.Item1, t.Item2 + x) :
                                new Tuple<StringBuilder, int>(t.Item1.AppendLine("Insufficient capacity!"), t.Item2)
                            ) :
                            new Tuple<StringBuilder, int>(t.Item1.AppendLine(t.Item2.ToString()), 0)
                    )
                    .Item1
                    .ToString()
            );
        }
    }
}
