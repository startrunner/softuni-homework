﻿using System;
using System.Linq;
using System.Text;

namespace _10SumOfChars
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(
                "The sum equals: " +
                Enumerable.Repeat(0, int.Parse(Console.ReadLine()))
                .Select(
                    x => Encoding.ASCII.GetBytes(Console.ReadLine())
                    .Select(y => (int)y)
                    .Sum()
                )
                .Sum()
            );
        }
    }
}
