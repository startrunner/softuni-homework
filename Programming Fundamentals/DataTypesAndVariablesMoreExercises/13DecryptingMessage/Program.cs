﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13DecryptingMessage
{
    class Program
    {
        static void Main(string[] args)
        {
            Enumerable
                .Repeat(new Tuple<int, int>(int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine())), 1)
                .Select(tuple =>
                    Enumerable
                        .Repeat(0, tuple.Item2)
                        .Select(x => (char)(Console.ReadLine().First() + tuple.Item1))
                )
                .Select(x => string.Join("", x))
                .ToList()
                .ForEach(x => Console.WriteLine(x));
        }
    }
}
