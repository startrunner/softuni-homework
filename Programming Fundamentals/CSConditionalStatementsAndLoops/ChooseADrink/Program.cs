﻿using System;
using System.Collections.Generic;

namespace ChooseADrink
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine(new Dictionary<string, string>() {
                    { "Athlete", "Water"},
                    { "Businessman", "Coffee"},
                    { "Businesswoman", "Coffee"},
                    { "SoftUni Student", "Beer"}
                }[Console.ReadLine()]);
            }
            catch
            {
                Console.WriteLine("Tea");
            }
        }
    }
}
