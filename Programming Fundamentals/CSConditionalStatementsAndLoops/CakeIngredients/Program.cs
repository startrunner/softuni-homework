﻿using System;

namespace CakeIngredients
{
    class Program
    {
        static void Main(string[] args)
        {
            int ingredientCount=0;
            for(;;)
            {
                string ingredient = Console.ReadLine();
                if (ingredient != "Bake!")
                {
                    ingredientCount++;
                    Console.WriteLine($"Adding ingredient {ingredient}.");
                }
                else break;
            }
            Console.WriteLine($"Preparing cake with {ingredientCount} ingredients.");
        }
    }
}
