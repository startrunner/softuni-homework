﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DifferentNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = int.Parse(Console.ReadLine()), b = int.Parse(Console.ReadLine());
            bool valid=false;


            for(int i=a;i<=b;i++)
            {
                for(int j=i+1;j<=b;j++)
                {
                    for(int k=j+1;k<=b;k++)
                    {
                        for(int l=k+1;l<=b;l++)
                        {
                            for(int m=l+1;m<=b;m++)
                            {
                                Console.WriteLine(string.Join(" ", new int[] { i, j, k, l, m }));
                                valid = true;
                            }
                        }
                    }
                }
            }

            if (!valid) Console.WriteLine("No");
        }
    }
}
