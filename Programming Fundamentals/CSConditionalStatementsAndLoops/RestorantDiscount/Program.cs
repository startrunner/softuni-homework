﻿using System;
using System.Collections.Generic;

namespace RestorantDiscount
{
    class Program
    {
        static void Main(string[] args)
        {
            int groupSize = int.Parse(Console.ReadLine());
            string package = Console.ReadLine();

            string hallName = null;
            double hallPrice=0;
            if (groupSize <= 50)
            {
                hallPrice = 2500;
                hallName = "Small Hall";
            }
            else if (groupSize <= 100)
            {
                hallPrice = 5000;
                hallName = "Terrace";
            }
            else if (groupSize <= 120)
            {
                hallPrice = 7500;
                hallName = "Great Hall";
            }
            else
            {
                Console.WriteLine("We do not have an appropriate hall.");
                return;
            }

            Dictionary<string, double> packagePrices = new Dictionary<string, double>() {
                { "Normal", 500},
                { "Gold", 750},
                { "Platinum", 1000}
            };

            Dictionary<string, double> rates = new Dictionary<string, double>() {
                { "Normal", .95},
                { "Gold", .90},
                { "Platinum", .85}
            };

            double totalPrice = (hallPrice + packagePrices[package]) * rates[package];

            Console.WriteLine($"We can offer you the {hallName}");
            Console.WriteLine($"The price per person is {(totalPrice / groupSize).ToString("0.00")}$");
        }
    }
}
