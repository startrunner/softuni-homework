﻿using System;
using System.Linq;

namespace WordInPlural
{
    class Program
    {
        static void Main(string[] args)
        {
            string word = Console.ReadLine();

            if (new string[] { "o", "ch", "s", "sh", "x", "z" }.Any(suffix => word.EndsWith(suffix))) word += "es";
            else if (word.EndsWith("y")) word = word.Substring(0, word.Length - 1) + "ies";
            else word += "s";

            Console.WriteLine(word);
        }
    }
}
