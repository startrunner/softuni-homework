﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel
{
    class Program
    {
        static void Main(string[] args)
        {
            string month = Console.ReadLine();
            int nightsCount = int.Parse(Console.ReadLine());

            Dictionary<string, double>
                studioPrices = new Dictionary<string, double>(),
                doublePrices = new Dictionary<string, double>(),
                suitePrices = new Dictionary<string, double>();

            studioPrices["May"] = studioPrices["October"] = 50;
            studioPrices["June"] = studioPrices["September"] = 60;
            studioPrices["July"] = studioPrices["August"] = studioPrices["December"] = 68;

            doublePrices["May"] = doublePrices["October"] = 65;
            doublePrices["June"] = doublePrices["September"] = 72;
            doublePrices["July"] = doublePrices["August"] = doublePrices["December"] = 77;

            suitePrices["May"] =  suitePrices["October"] = 75;
            suitePrices["June"] = suitePrices["September"] = 82;
            suitePrices["July"] = suitePrices["August"] = suitePrices["December"] = 89;

            double studioPrice = studioPrices[month] * nightsCount;
            double doublePrice = doublePrices[month] * nightsCount;
            double suitePrice = suitePrices[month] * nightsCount;


            if (nightsCount > 7)
            {
                if(new string[] { "September", "October"}.Contains(month))
                {
                    studioPrice -= studioPrices[month];
                }
                if (new string[] { "May", "October" }.Contains(month))
                {
                    studioPrice *= 0.95;
                }
            }
            if(nightsCount>14)
            {
                if(new string[] { "June", "September"}.Contains(month))
                {
                    doublePrice *= 0.9;
                }
                if(new string[] {"July", "August", "December"}.Contains(month))
                {
                    suitePrice *= 0.85;
                }
            }

            Console.WriteLine($"Studio: {studioPrice:0.00} lv.");
            Console.WriteLine($"Double: {doublePrice:0.00} lv.");
            Console.WriteLine($"Suite: {suitePrice:0.00} lv.");
        }
    }
}
