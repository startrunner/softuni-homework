﻿using System;

namespace GameOfNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int 
                n = int.Parse(Console.ReadLine()),
                m = int.Parse(Console.ReadLine()),
                mn = int.Parse(Console.ReadLine());

            for(int i=m;i>=n;i--)
            {
                for(int j=m;j>=n;j--)
                {
                    if(i+j==mn && i!=j)
                    {
                        Console.WriteLine($"Number found! {i} + {j} = {mn}");
                        return;
                    }
                }
            }

            Console.WriteLine($"{(m - n + 1) * (m - n + 1)} combinations - neither equals {mn}");
        }
    }
}
