﻿using System;

namespace NeighborWars
{
    class Program
    {
        static void Main(string[] args)
        {
            int peshoAttack = int.Parse(Console.ReadLine());
            int goshoAttack = int.Parse(Console.ReadLine());

            int peshoHealth = 100, goshoHealth = 100;

            for (int round = 1; ; round++)
            {
                if (round % 2 == 0)
                {
                    peshoHealth -= goshoAttack;

                    if (peshoHealth <= 0)
                    {
                        Console.WriteLine($"Gosho won in {round}th round.");
                        return;
                    }
                    Console.WriteLine($"Gosho used Thunderous fist and reduced Pesho to {peshoHealth} health.");
                }
                else
                {
                    goshoHealth -= peshoAttack;
                    

                    if (goshoHealth <= 0)
                    {
                        Console.WriteLine($"Pesho won in {round}th round.");
                        return;
                    }
                    Console.WriteLine($"Pesho used Roundhouse kick and reduced Gosho to {goshoHealth} health.");
                }
                if (round % 3 == 0)
                {
                    peshoHealth += 10;
                    goshoHealth += 10;
                }

            }
        }
    }
}
