﻿using System;

namespace CountTheIntegers
{
    class Program
    {
        static void Main(string[] args)
        {
            int ans = 0, x=0;
            while (int.TryParse(Console.ReadLine(), out x)) ans++;
            Console.WriteLine(ans);
        }
    }
}
