﻿using System;
using System.Linq;
using System.Text;

namespace TriangleOfNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(
                Enumerable
                    .Range(1, int.Parse(Console.ReadLine()))
                    .Select(x => string.Join(" ", Enumerable.Repeat(x, x)))
                    .Aggregate(new StringBuilder(), (builder, s) => builder.AppendLine(s))
            );
        }
    }
}
