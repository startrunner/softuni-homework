﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CaloriesCounter
{
    class Program
    {
        static void Main(string[] args)
        {
            var calories = new Dictionary<string, int>() {
                {"CHEESE", 500},
                {"TOMATO SAUCE".Normalize(), 150},
                {"SALAMI".Normalize(), 600},
                {"PEPPER".Normalize(), 50}
            };

            Console.WriteLine("Total calories: " + Enumerable
                .Repeat(0, int.Parse(Console.ReadLine()))
                .Select(x => Console.ReadLine().ToUpper())
                .Select(x => calories.ContainsKey(x) ? calories[x] : 0)
                .Sum());
        }
    }
}
