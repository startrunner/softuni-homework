﻿using System;
using System.Collections.Generic;

namespace ChooseADrink
{
    class Program
    {
        static void Main(string[] args)
        {
            string drink;
            string profession = Console.ReadLine();

            try
            {
                drink=(new Dictionary<string, string>() {
                    { "Athlete", "Water"},
                    { "Businessman", "Coffee"},
                    { "Businesswoman", "Coffee"},
                    { "SoftUni Student", "Beer"}
                }[profession]);
            }
            catch
            {
                drink = "Tea";
            }

            var prices = new Dictionary<string, double>() {
                { "Water", 0.7},
                { "Coffee", 1},
                { "Beer", 1.7},
                { "Tea", 1.2}
            };

            double quantity = double.Parse(Console.ReadLine());

            Console.WriteLine($"The {profession} has to pay {(quantity * prices[drink]).ToString("0.00")}.");
        }
    }
}
