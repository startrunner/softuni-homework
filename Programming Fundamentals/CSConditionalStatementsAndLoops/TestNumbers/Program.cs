﻿using System;

namespace TestNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int m = int.Parse(Console.ReadLine());
            int combs = 0;
            int boundary = int.Parse(Console.ReadLine());

            int sum = 0;

            for (int i = n; i >= 1; i--)
            {
                for (int j = 1; j <= m; j++)
                {
                    {
                        combs++;
                        int x = i * j * 3;
                        sum += x;
                        //Console.WriteLine($"{i} {j}-> {sum}");
                        if (sum >= boundary) { i = 0; break; }
                    }
                }
            }

            Console.WriteLine($"{combs} combinations");
            Console.WriteLine($"Sum: {sum} {(sum >= boundary?$">= {boundary}":"")}");
        }
    }
}
