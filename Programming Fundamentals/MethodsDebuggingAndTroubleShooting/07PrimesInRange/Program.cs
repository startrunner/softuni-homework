﻿using System;
using System.Linq;

namespace _07PrimesInRange
{
    class Program
    {
        static bool IsPrime(int n)
        {
            return n > 1 &&
                                !Enumerable
                                    .Range(2, (int)n - 2)
                                    .Any(x => n % x == 0);
        }

        static void Main(string[] args)
        {
            int a = int.Parse(Console.ReadLine()), b = int.Parse(Console.ReadLine());
            Console.WriteLine(
                string.Join(
                    ", ",
                    Enumerable
                        .Range(a, b - a + 1)
                        .Where(x => IsPrime(x))
                )
            );
        }
    }
}
