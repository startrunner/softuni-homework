﻿using System;
using System.Linq;

namespace _09LongerLine
{
    class Program
    {
        static void Main(string[] args)
        {
            Enumerable
                .Repeat(0, 2)
                .Select(
                    x => Enumerable
                    .Repeat(0, 2)
                    .Select(y => new Tuple<double, double>(double.Parse(Console.ReadLine()), double.Parse(Console.ReadLine())))
                    .ToArray()
                )
                .OrderByDescending(x => Math.Pow(x[0].Item1 - x[1].Item1, 2) + Math.Pow(x[0].Item2 - x[1].Item2, 2))
                .Take(1)
                .Select(x=>x.OrderBy(y=>y.Item1*y.Item1 + y.Item2*y.Item2).ToArray())
                .ToList()
                .ForEach(x => Console.WriteLine($"{x[0]}{x[1]}"));
        }
    }
}
