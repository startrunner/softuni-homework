﻿using System;
using System.Linq;

namespace _06PrimeChecker
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Enumerable
                    .Repeat(long.Parse(Console.ReadLine()), 1)
                    .ToList()
                    .ForEach(
                        n =>
                            Console.WriteLine(
                                n > 1 &&
                                !Enumerable
                                    .Range(2, (int)n - 2)
                                    .Any(x => n % x == 0))
                    );
            }
            catch
            {
                Console.WriteLine("True");
            }
        }
    }
}
