﻿using System;
using System.Linq;
using System.Numerics;

namespace _14FactorialTrailingZeros
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(
                Enumerable
                    .Range(1, int.Parse(Console.ReadLine()))
                    .Aggregate(
                        new BigInteger(1),
                        (i, x) => i * x
                    )
                    .ToString()
                    .Reverse()
                    .TakeWhile(x=>x=='0')
                    .Count()
            );
        }
    }
}
