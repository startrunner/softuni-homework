﻿using System;
using System.IO;
using System.Linq;

namespace _12MasterNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            for (int i = 1; i <= n; i++)
            {
                string str = i.ToString();
                bool pali = true;
                for (int j = 0, k = str.Length - 1; j < k; j++, k--)
                {
                    if (str[j] != str[k])
                    {
                        pali = false;
                        break;
                    }
                }

                if(pali)
                {
                    bool evenDigit = false;
                    int digSum = str.Select(x => { if ((x - '0') % 2 == 0) evenDigit = true; return (x - '0'); }).Sum();
                    if(digSum%7==0 && evenDigit)
                    {
                        Console.WriteLine(str);
                    }
                }
            }
        }
    }
}

            /*Enumerable
                .Range(1, int.Parse(Console.ReadLine()))
                .Where(x => Enumerable.SequenceEqual(x.ToString(), x.ToString().Reverse()))
                .Where(x => x.ToString().Select(y => y - '0').Sum() % 7 == 0)
                .Where(x => x.ToString().Any(y => (y - '0') % 2 == 0))
                .Aggregate(
                    new StreamWriter(Console.OpenStandardOutput()),
                    (w, x) => { w.WriteLine(x); return w; }
                )
                .Close();*/
