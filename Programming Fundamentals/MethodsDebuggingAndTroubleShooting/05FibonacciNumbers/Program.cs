﻿using System;
using System.Linq;

namespace _05FibonacciNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(
                Enumerable
                    .Repeat(0, int.Parse(Console.ReadLine()))
                    .Aggregate(
                        new Tuple<ulong, ulong>(1, 1),
                        (t, x) => new Tuple<ulong, ulong>(t.Item2, t.Item1 + t.Item2)
                    )
                    .Item1
            );
        }
    }
}
