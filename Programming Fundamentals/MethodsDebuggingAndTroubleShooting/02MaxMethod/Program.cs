﻿using System;

namespace _02MaxMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Math.Max(Math.Max(int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine())), int.Parse(Console.ReadLine())));
        }
    }
}
