﻿using System;

public class Substring
{
	public static void Main()
	{
		string text = Console.ReadLine();
		int jump = int.Parse(Console.ReadLine());

        bool any = false;

		for(int i=0;i<text.Length;i++)
        {
            if(text[i]=='p')
            {
                Console.WriteLine(text.Substring(i, Math.Min(jump + 1, text.Length - i)));

                i += jump;
                any = true;
            }
        }

        if (!any) Console.WriteLine("no");
	}
}
