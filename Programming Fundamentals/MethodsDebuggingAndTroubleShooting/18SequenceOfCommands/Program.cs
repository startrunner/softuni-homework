﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _18SequenceOfCommands
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            List<int> arr = new List<int>();
            Console.ReadLine().Split().Select(x => int.Parse(x)).ToList().ForEach(x => arr.Add(x));

            for(;;)
            {
                string[] line = Console.ReadLine().Split(null as char[], StringSplitOptions.RemoveEmptyEntries);
                if (line[0] == "stop") break;
                int i, x;
                switch(line[0])
                {
                    case "add":
                        i = int.Parse(line[1])-1;
                        x = int.Parse(line[2]);
                        arr[i] += x;
                        break;
                    case "subtract":
                        i = int.Parse(line[1])-1;
                        x = int.Parse(line[2]);
                        arr[i] -= x;
                        break;
                    case "multiply":
                        i = int.Parse(line[1])-1;
                        x = int.Parse(line[2]);
                        arr[i] *= x;
                        break;
                    case "rshift":
                        x = arr.Last();
                        arr.RemoveAt(arr.Count - 1);
                        arr.Insert(0, x);
                        break;
                    case "lshift":
                        x = arr.First();
                        arr.RemoveAt(0);
                        arr.Add(x);
                        break;
                }

                foreach (var y in arr) Console.Write($"{y} ");
                Console.WriteLine();

            }
        }
    }
}
