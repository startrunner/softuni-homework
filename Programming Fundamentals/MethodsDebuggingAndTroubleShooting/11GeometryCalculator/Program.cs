﻿using System;

namespace _11GeometryCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            switch (Console.ReadLine())
            {
                case "triangle":
                    Console.WriteLine($"{double.Parse(Console.ReadLine()) * double.Parse(Console.ReadLine()) / 2:0.00}");
                    break;
                case "rectangle":
                    Console.WriteLine($"{double.Parse(Console.ReadLine()) * double.Parse(Console.ReadLine()):0.00}");
                    break;
                case "circle":
                    Console.WriteLine($"{Math.PI * Math.Pow(double.Parse(Console.ReadLine()), 2):0.00}");
                    break;
                case "square":
                    Console.WriteLine($"{Math.Pow(double.Parse(Console.ReadLine()), 2):0.00}");
                    break;
            }
        }
    }
}
