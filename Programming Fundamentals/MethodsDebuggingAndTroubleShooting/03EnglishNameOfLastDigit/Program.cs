﻿using System;
using System.Linq;

namespace _03EnglishNameOfLastDigit
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(
                new string[]{
                    "zero",
                    "one",
                    "two",
                    "three",
                    "four",
                    "five",
                    "six",
                    "seven",
                    "eight",
                    "nine"
                }[int.Parse(Console.ReadLine().Last().ToString())]
            );
        }
    }
}
