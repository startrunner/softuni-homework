﻿using System;
using System.Linq;
using System.Numerics;

namespace _13Factorial
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(
                Enumerable
                    .Range(1, int.Parse(Console.ReadLine()))
                    .Aggregate(
                        new BigInteger(1),
                        (i, x) => i * x
                    )
            );
        }
    }
}
