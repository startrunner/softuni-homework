﻿using System;

namespace _10CubeProperties
{
    class Program
    {
        static void Main(string[] args)
        {
            double x = double.Parse(Console.ReadLine());

            switch(Console.ReadLine())
            {
                case "face":
                    Console.WriteLine($"{Math.Sqrt(2 * x * x):0.00}");
                    break;
                case "space":
                    //Console.WriteLine($"{Math.Sqrt(Math.Sqrt(2 * x * x) + x * x):0.00}");
                    Console.WriteLine($"{Math.Sqrt(3 * x * x):0.00}");
                    break;
                case "volume":
                    Console.WriteLine($"{x*x*x:0.00}");
                    break;
                case "area":
                    Console.WriteLine($"{6*x*x:0.00}");
                    break;
                default:
                    throw null;
            }
        }
    }
}
