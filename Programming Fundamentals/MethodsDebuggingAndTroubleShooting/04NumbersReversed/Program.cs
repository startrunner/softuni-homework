﻿using System;
using System.Linq;

namespace _04NumbersReversed
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(new string(Console.ReadLine().Reverse().ToArray()));
        }
    }
}
