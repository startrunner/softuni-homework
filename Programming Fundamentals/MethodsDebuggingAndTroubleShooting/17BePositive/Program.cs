﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _17BePositive
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            for (int i = 0; i < n; i++)
            {

                Queue<int> q = new Queue<int>();
                int[] nums = Console.ReadLine().Split(null as char[], StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToArray();
                foreach (var x in nums) q.Enqueue(x);

                bool any = false;

                while (q.Any())
                {
                    int x = q.Dequeue();
                    if (x >= 0)
                    {
                        Console.Write($"{x} ");
                        any = true;
                    }
                    else if(q.Any())
                    {
                        int y = q.Dequeue();
                        x += y;
                        if(x>=0)
                        {
                            Console.Write($"{x} ");
                            any = true;
                        }
                    }
                }
                if (!any) Console.Write("(empty)");
                Console.WriteLine();
            }
        }
    }
}
