﻿using System;
using System.Linq;

namespace _08CenterPoint
{
    class Program
    {
        static void Main(string[] args)
        {
            Enumerable
                .Repeat(0, 2)
                .Select(
                    x => new Tuple<double, double>(double.Parse(Console.ReadLine()), double.Parse(Console.ReadLine()))
                )
                .OrderBy(x => x.Item1 * x.Item1 + x.Item2 * x.Item2)
                .Take(1)
                .ToList()
                .ForEach(x => Console.WriteLine(x));

        }
    }
}
