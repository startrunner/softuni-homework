﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16InstructionSet
{
    class Program
    {
        static void Main(string[] args)
        {
            for (;;)
            {
                string[] command = Console.ReadLine().Split();

                long op1, op2;
                switch (command[0])
                {
                    case "INC":
                        op1 = long.Parse(command[1]);
                        Console.WriteLine(op1 + 1);
                        break;
                    case "DEC":
                        op1 = long.Parse(command[1]);
                        Console.WriteLine(op1 - 1);
                        break;
                    case "ADD":
                        op1 = long.Parse(command[1]);
                        op2 = long.Parse(command[2]);
                        Console.WriteLine(op1 + op2);
                        break;
                    case "MLA":
                        op1 = long.Parse(command[1]);
                        op2 = long.Parse(command[2]);
                        Console.WriteLine(op1 * op2);
                        break;
                    default:
                        command = null;
                        break;
                }
                if (command == null) break;
            }
        }
    }
}
