﻿using System;

namespace _05CompareCharArrays
{
    class Program
    {
        static bool Compare(string a, string b)
        {
            for(int i=0; ;i++)
            {
                if (i == a.Length) return true;
                if (i == b.Length) return false;
                if (a[i] < b[i]) return true;
                if (a[i] > b[i]) return false;
            }
        }

        static void Main(string[] args)
        {
            string a = Console.ReadLine().Replace(" ", "");
            string b = Console.ReadLine().Replace(" ", "");

            if(Compare(a, b))
            {
                Console.WriteLine(a);
                Console.WriteLine(b);
            }
            else
            {
                Console.WriteLine(b);
                Console.WriteLine(a);
            }
        }
    }
}
