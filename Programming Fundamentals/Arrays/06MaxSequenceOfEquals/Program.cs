﻿using System;
using System.Linq;

namespace _06MaxSequenceOfEquals
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] arr = Console.ReadLine().Split();

            string item = arr.First();
            int times = 1;

            for(int i=1, ct=1;i<arr.Length;i++)
            {
                if(arr[i]==arr[i-1])
                {
                    ct++;
                }
                else
                {
                    ct = 1;
                }

                if(ct>times)
                {
                    times = ct;
                    item = arr[i];
                }
            }

            for (int i = 0; i < times; i++) Console.Write($"{item} ");
            Console.WriteLine();
        }
    }
}
