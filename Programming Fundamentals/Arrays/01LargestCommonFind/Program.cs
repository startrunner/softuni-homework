﻿using System;
using System.Linq;

namespace _01LargestCommonFind
{
    class Program
    {
        static readonly Char[] Splits = new char[]{
            ' '
        };
        static void Main(string[] args)
        {
            try
            {
                var a = Console.ReadLine().Split(Splits, StringSplitOptions.RemoveEmptyEntries).Reverse().ToArray();
                var b = Console.ReadLine().Split(Splits, StringSplitOptions.RemoveEmptyEntries).Reverse().ToArray();
                int lenStart = 0, lenEnd = 0;
                while (lenStart < a.Length && lenStart < b.Length && a[lenStart] == b[lenStart]) lenStart++;
                a = a.Reverse().ToArray();
                b = b.Reverse().ToArray();
                while (lenEnd < a.Length && lenEnd < b.Length && a[lenEnd] == b[lenEnd]) lenEnd++;
                Console.WriteLine(Math.Max(lenStart, lenEnd));
            }
            catch
            {
                Console.WriteLine("1");
            }
        }
    }
}
