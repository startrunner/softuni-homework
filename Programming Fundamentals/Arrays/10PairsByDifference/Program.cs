﻿using System;
using System.Linq;

namespace _10PairsByDifference
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = Console.ReadLine().Split().Select(x => int.Parse(x)).ToArray();
            int diff = int.Parse(Console.ReadLine());

            int ans = 0;
            for(int i=0;i<a.Length;i++)
            {
                for(int j=i+1;j<a.Length;j++)
                {
                    if (Math.Abs(a[i] - a[j]) == diff) ans++;
                }
            }

            Console.WriteLine(ans);
        }
    }
}
