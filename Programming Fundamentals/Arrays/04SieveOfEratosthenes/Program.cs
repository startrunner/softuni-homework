﻿using System;
using System.Linq;

namespace _04SieveOfEratosthenes
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            bool[] sieve = Enumerable.Repeat(true, n+1).ToArray();

            sieve[0] = sieve[1] = false;
            for(int i=2;i<=n;i++)
            {
                if(sieve[i])
                {
                    for(int j=2*i;j<=n;j+=i)
                    {
                        sieve[j] = false;
                    }
                }
            }

            sieve.
                Select((x, i) => x ? i : -1).
                Where(x => x != -1)
                .ToList()
                .ForEach(x => Console.Write($"{x} "));
            Console.WriteLine();

        }
    }
}
