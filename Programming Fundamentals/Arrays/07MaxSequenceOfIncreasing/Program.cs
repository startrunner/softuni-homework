﻿using System;
using System.Linq;

namespace _06MaxSequenceOfEquals
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = Console.ReadLine().Split().Select(x=>int.Parse(x)).ToArray();

            int startIndex = 0;
            int times = 1;

            for (int i = 1, ct = 1, csi=0; i < arr.Length; i++)
            {
                if (arr[i] > arr[i - 1])
                {
                    ct++;
                }
                else
                {
                    ct = 1;
                    csi = i;
                }

                if (ct > times)
                {
                    times = ct;
                    startIndex = csi;
                }
            }

            for (int i = 0; i < times; i++) Console.Write($"{arr[i + startIndex]} ");
            Console.WriteLine();
        }
    }
}
