﻿using System;
using System.Linq;

namespace _03FoldAndSum
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = Console.ReadLine().Split().Select(x => int.Parse(x)).ToArray();
            int k = arr.Length / 4;

            for(int i=0;i<k;i++)
            {
                arr[2 * k - i - 1] += arr[i];
                arr[2 * k + i] += arr[arr.Length - i - 1];
            }

            Console.WriteLine(string.Join(" ", arr.Skip(k).Take(2 * k)));
        }
    }
}
