﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11EqualSums
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = Console.ReadLine().Split().Select(x => int.Parse(x)).ToArray();

            bool any = false;
            for (int i = 0, leftSum = 0, allSum = arr.Sum(); i < arr.Length; leftSum += arr[i], i++)
            {
                int rightSum = allSum - leftSum - arr[i];
                if (leftSum == rightSum)
                {
                    Console.WriteLine(i);
                    any = true;
                    break;
                }
            }
            if (!any) Console.WriteLine("no");
        }
    }
}
