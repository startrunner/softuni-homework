﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _06MaxSequenceOfEquals
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = Console.ReadLine().Split().Select(x=>int.Parse(x)).ToArray();

            Dictionary<int, int> countOf = new Dictionary<int, int>();

            foreach(int x in arr)
            {
                if (countOf.ContainsKey(x)) countOf[x]++;
                else countOf[x] = 1;
            }

            int maxCount = countOf.Values.Max();

            Console.WriteLine(arr.First(x => maxCount == countOf[x]));
        }
    }
}
