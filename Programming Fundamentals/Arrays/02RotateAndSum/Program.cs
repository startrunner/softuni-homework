﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02RotateAndSum
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = Console.ReadLine().Split().Select(x => int.Parse(x)).ToArray();
            int[] sum = Enumerable.Repeat(0, arr.Length).ToArray();
            int k = int.Parse(Console.ReadLine());
            
            for(int i=0;i<arr.Length;i++)
            {
                for(int j=1;j<=k;j++)
                {
                    int pos = (i + j) % arr.Length;
                    sum[pos] += arr[i];
                }
            }

            Console.WriteLine(string.Join(" ", sum));
        }
    }
}
