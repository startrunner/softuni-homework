﻿using System;

namespace _09IndexOfLetters
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach(char x in Console.ReadLine().ToLower().Trim())
            {
                Console.WriteLine($"{x} -> {x-'a'}");
            }
        }
    }
}
