﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _05ParkingValidation
{
    class Program
    {
        static void Main(string[] args)
        {
            HashSet<string> allPlates = new HashSet<string>();
            Dictionary<string, string> userPlates = new Dictionary<string, string>();

            int n = int.Parse(Console.ReadLine());

            for (int i = 0; i < n; i++)
            {
                string[] line = Console.ReadLine().Split();

                string username = line[1];

                if(line[0]=="register")
                {
                    string plate = line[2];

                    if(userPlates.ContainsKey(username))
                    {
                        Console.WriteLine($"ERROR: already registered with plate number {userPlates[username]}");
                        continue;
                    }
                    else if(allPlates.Contains(plate))
                    {
                        Console.WriteLine($"ERROR: license plate {plate} is busy");
                        continue;
                    }
                    else if(plate.Length!=8 || !plate.Substring(0, 2).All(char.IsUpper) || !plate.Substring(6, 2).All(char.IsUpper) || !plate.Skip(2).Take(4).All(x=>char.IsDigit(x)))
                    {
                        Console.WriteLine($"ERROR: invalid license plate {plate}");
                    }
                    else
                    {
                        allPlates.Add(plate);
                        userPlates[username] = plate;
                        Console.WriteLine($"{username} registered {plate} successfully");
                    }
                 }
                else
                {
                    if(userPlates.ContainsKey(username))
                    {
                        userPlates.Remove(username);
                        Console.WriteLine($"user {username} unregistered successfully");
                    }
                    else
                    {
                        Console.WriteLine($"ERROR: user {username} not found");
                    }
                }
            }

            userPlates
                .ToList()
                .ForEach(x => Console.WriteLine($"{x.Key} => {x.Value}"));
        }
    }
}
