﻿using System;
using System.Linq;
using System.Text;

namespace _06ByteFlip
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(
                Encoding.ASCII.GetString(
                    Console
                        .ReadLine()
                        .Split()
                        .Where(x=>x.Length==2)
                        .Select(x=>new string(x.Reverse().ToArray()))
                        .Select(x=>int.Parse(x, System.Globalization.NumberStyles.HexNumber))
                        .Reverse()
                        .Select(x=>(byte)x)
                        .ToArray()
                )
            );
        }
    }
}
