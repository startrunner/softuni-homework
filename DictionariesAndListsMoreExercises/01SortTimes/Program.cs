﻿using System;
using System.Linq;

namespace _01SortTimes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(
                string.Join(
                    ", ",
                    Console.ReadLine()
                    .Split(' ')
                    .Select(x=>TimeSpan.Parse(x))
                    .OrderBy(x=>x.TotalSeconds)
                    .Select(x=>$"{x.Hours:00}:{x.Minutes:00}")
                )
            );
        }
    }
}
