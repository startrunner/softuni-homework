﻿using System;
using System.Linq;
using System.Text;

namespace _07TakeOrSkipRope
{
    class Program
    {
        static void Main(string[] args)
        {
            string line = Console.ReadLine();

            int[] digits = line.Where(char.IsDigit).Select(x => x - '0').ToArray();
            char[] txt = line.Where(x => !char.IsDigit(x)).ToArray();

            int[] takeList = digits.Where((x, i) => i % 2 == 0).ToArray();
            int[] skipList = digits.Where((x, i) => i % 2 == 1).ToArray();

            StringBuilder builder = new StringBuilder();

            int skipped = 0;

            for (int i = 0; i < takeList.Length; i++)
            {
                int skip = skipList[i], take = takeList[i];

                string str = new string(
                    txt
                        .Skip(skipped)
                        .Take(take)
                        .ToArray()
                );

                builder.Append(str);
                skipped += skip + take;
            }

            Console.WriteLine(builder);
        }
    }
}
