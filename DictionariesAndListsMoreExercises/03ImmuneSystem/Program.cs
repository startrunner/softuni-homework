﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _03ImmuneSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            double initialHealth = double.Parse(Console.ReadLine());
            double health = initialHealth;

            HashSet<string> done = new HashSet<string>();

            for (;;)
            {
                string virus = Console.ReadLine();
                if (virus == "end") break;

                double strenght = Encoding
                    .ASCII
                    .GetBytes(virus)
                    .Select(x => (int)x)
                    .Sum() / 3;


                TimeSpan timeToDefeat;

                if (!done.Contains(virus))
                {
                    timeToDefeat = TimeSpan.FromSeconds(strenght * virus.Length);
                    done.Add(virus);
                }
                else
                {
                    timeToDefeat = TimeSpan.FromSeconds(strenght * virus.Length / 3.0);
                }

                Console.WriteLine($"Virus {virus}: {strenght} => {(int)timeToDefeat.TotalSeconds} seconds");

                if (timeToDefeat.TotalSeconds > health)
                {
                    Console.WriteLine("Immune System Defeated.");
                    Environment.Exit(0);
                }
                else
                {
                    health -= timeToDefeat.TotalSeconds;
                    health = (int)health;
                    Console.WriteLine($"{virus} defeated in {(int)timeToDefeat.TotalMinutes}m {timeToDefeat.Seconds}s.");
                    Console.WriteLine($"Remaining health: {(int)health}");
                    health = Math.Min(initialHealth, health * 1.2);
                }
            }
            Console.WriteLine($"Final Health: {(int)health}");
        }
    }
}
