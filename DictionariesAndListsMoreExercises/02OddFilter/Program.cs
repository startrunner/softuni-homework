﻿using System;
using System.Linq;

namespace _02OddFilter
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbers = Console
                .ReadLine()
                .Split()
                .Select(int.Parse)
                .Where(x=>x%2==0)
                .ToArray();

            double avg = numbers.Average();

            numbers = numbers.Select(x => x < avg ? x - 1 : x + 1).ToArray();

            Console.WriteLine(string.Join(" ", numbers));
        }
    }
}
