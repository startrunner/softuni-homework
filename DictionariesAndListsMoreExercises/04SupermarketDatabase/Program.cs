﻿using System;
using System.Collections.Generic;

namespace _04SupermarketDatabase
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, double> priceOf = new Dictionary<string, double>();
            Dictionary<string, int> quantityOf = new Dictionary<string, int>();

            for(;;)
            {
                string line = Console.ReadLine();
                if (line == "stocked") break;
                string[] split = line.Split();

                string name = split[0];
                double price = double.Parse(split[1]);
                int quantity = int.Parse(split[2]);

                if(!quantityOf.ContainsKey(name))
                {
                    quantityOf[name] = quantity;
                }
                else
                {
                    quantityOf[name] += quantity;
                }
                priceOf[name] = price;
            }

            double total = 0;

            foreach(string name in priceOf.Keys)
            {
                double price = priceOf[name];
                int quantity = quantityOf[name];
                total += price * quantity;
                Console.WriteLine($"{name}: ${price:F2} * {quantity} = ${price * quantity:F2}");
            }

            Console.WriteLine(new string('-', 30));

            Console.WriteLine($"Grand Total: ${total:F2}");
        }
    }
}
