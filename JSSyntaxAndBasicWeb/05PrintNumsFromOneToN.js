function printFromOneToN(args) {
    let n=Number(args[0]);

    for(let i=1;i<=n;i++) {
        process.stdout.write(i.toString()+'\n');
    }
}