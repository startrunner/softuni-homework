function printNumsReversed(args) {
    for(x of args.reverse()){
        process.stdout.write(x.toString()+'\n');
    }
}