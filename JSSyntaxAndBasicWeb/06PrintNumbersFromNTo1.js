function printFromNToOne(args) {
    let n=Number(args[0]);

    for(let i=n;i!=0;i--) {
        process.stdout.write(i.toString()+'\n');
    }
}