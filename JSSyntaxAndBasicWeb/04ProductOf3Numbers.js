function productOf3(args) {
    let zeros=0;
    let neg = 0;


    for(let i=0;i<3;i++) {
        let x=Number(args[i]);

        if(x==0) {
            zeros++;
            break;
        }
        else if(x<0){
            neg++;
        }
    }

    if(zeros!=0 || neg%2==0){
        return "Positive";
    }
    else{
        return "Negative";
    }
}