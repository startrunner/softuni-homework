function multiplyOrDivide(args) {
    let n=Number(args[0]);
    let x=Number(args[1]);

    if(x>=n) {
        return x*n;
    }
    else {
        return n/x;
    }
}