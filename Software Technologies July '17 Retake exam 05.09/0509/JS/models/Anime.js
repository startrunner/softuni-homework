const mongoose = require('mongoose');

let animeSchema = mongoose.Schema({
    name: {type: String, required: true, unique:false},
    description: {type: String, required:true, unique:false},
    watched: {type: String, required:true, unique:false},
    rating: {type: Number, required: true, unique: false}
});

let Anime = mongoose.model('Anime', animeSchema);

module.exports = Anime;