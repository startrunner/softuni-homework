﻿using System.Linq;
using System.Web.Mvc;
using AnimeList.Models;

namespace AnimeList.Controllers
{
    [ValidateInput(false)]
    public class AnimeController : Controller
    {
        [HttpGet]
        [Route("")]
        public ActionResult Index()
        {
            using(var db=new AnimeListDbContext())
            {
                var vm = db.Animes.ToList();
                return View(vm);
            }
        }

        [HttpGet]
        [Route("create")]
        public ActionResult Create() => View();

        [HttpPost]
        [Route("create")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Anime anime)
        {
            using(var db=new AnimeListDbContext())
            {
                db.Animes.Add(anime);
                db.SaveChanges();
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        [Route("delete/{id}")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction(nameof(Index));
            }

            using (var db = new AnimeListDbContext())
            {
                var vm = db.Animes.Single(x => x.Id == id);
                return View(vm);
            }
        }

        [HttpPost]
        [Route("delete/{id}")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirm(int? id, Anime animeModel)
        {
            if (id == null)
            {
                return RedirectToAction(nameof(Index));
            }

            using(var db=new AnimeListDbContext())
            {
                var anime = db.Animes.Single(x => x.Id == id);
                db.Animes.Remove(anime);
                db.SaveChanges();
            }
            return RedirectToAction(nameof(Index));
        }
    }
}